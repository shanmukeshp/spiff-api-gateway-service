package com.precorconnect.spiffapigatewayservice.webapi;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.precorconnect.PartnerSaleInvoiceNumber;
import com.precorconnect.PartnerSaleInvoiceNumberImpl;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.PartnerSaleRegIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReqImpl;

@Component
public class UploadPartnerSaleInvoiceReqWebRequestFactoryImpl implements
		UploadPartnerSaleInvoiceReqWebRequestFactory {

	@Override
	public UploadPartnerSaleInvoiceReq construct(
			String number,
			Long regId,
			MultipartFile multipartFile
			) {

		PartnerSaleRegId partnerSaleRegId =
							new PartnerSaleRegIdImpl(
									regId
									);

	    PartnerSaleInvoiceNumber partnerSaleInvoiceNumber =
	    							new PartnerSaleInvoiceNumberImpl(
	    									number
	    									);

	    File file = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") +
	    		multipartFile.getOriginalFilename());
	    try {
			multipartFile.transferTo(file);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		return
				new UploadPartnerSaleInvoiceReqImpl(
										partnerSaleInvoiceNumber,
										partnerSaleRegId,
										file
										);
	}

}
