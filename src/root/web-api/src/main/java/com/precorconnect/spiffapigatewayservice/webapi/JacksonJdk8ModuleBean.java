package com.precorconnect.spiffapigatewayservice.webapi;

import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.inject.Singleton;

@Component
class JacksonJdk8ModuleBean {

    @Bean
    @Singleton
    public Jdk8Module jdk8Module() {

        return new Jdk8Module();

    }
}
