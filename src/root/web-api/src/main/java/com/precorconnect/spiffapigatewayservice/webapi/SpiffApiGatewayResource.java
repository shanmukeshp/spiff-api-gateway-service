package com.precorconnect.spiffapigatewayservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.api.SpiffApiGatewayService;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.ClaimSpiffWebView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.PartnerRepInfoWebView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.PartnerSaleInvoiceWebView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.SpiffEntitlementWithPartnerRepInfoWebDto;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.SpiffEntitlementWithPartnerRepInfoWebView;


@RestController
@RequestMapping("/spiff-api-gateway")
@Api(value = "/spiff-api-gateway", description = "Operations on spiff api gateway")
public class SpiffApiGatewayResource {

	/*
    fields
     */
	private final OAuth2AccessTokenFactory oAuth2AccessTokenFactory;

	private final SpiffApiGatewayService spiffApiGatewayService;

	private final ClaimSpiffWebResponseFactory claimSpiffWebResponseFactory;

	private final PartnerRepInfoWebResponseFactory partnerRepInfoWebResponseFactory;

	private final PartnerSaleInvoiceWebResponseFactory partnerSaleInvoiceWebResponseFactory;

	private final SpiffEntitlementWithPartnerRepInfoWebResponseFactory spiffEntitlementWithPartnerRepInfoWebResponseFactory;

	private final SpiffEntitlementWithPartnerRepInfoWebRequestFactory spiffEntitlementWithPartnerRepInfoWebRequestFactory;

	private final UploadPartnerSaleInvoiceReqWebRequestFactory uploadPartnerSaleInvoiceReqWebRequestFactory;

	@Inject
	public SpiffApiGatewayResource(
			@NonNull final OAuth2AccessTokenFactory oAuth2AccessTokenFactory,
			@NonNull final SpiffApiGatewayService spiffApiGatewayService,
			@NonNull final ClaimSpiffWebResponseFactory claimSpiffWebResponseFactory,
			@NonNull final PartnerRepInfoWebResponseFactory partnerRepInfoWebResponseFactory,
			@NonNull final PartnerSaleInvoiceWebResponseFactory partnerSaleInvoiceWebResponseFactory,
			@NonNull final SpiffEntitlementWithPartnerRepInfoWebResponseFactory spiffEntitlementWithPartnerRepInfoWebResponseFactory,
			@NonNull final SpiffEntitlementWithPartnerRepInfoWebRequestFactory spiffEntitlementWithPartnerRepInfoWebRequestFactory,
			@NonNull final UploadPartnerSaleInvoiceReqWebRequestFactory uploadPartnerSaleInvoiceReqWebRequestFactory
			){

		this.oAuth2AccessTokenFactory =
                guardThat(
                        "oAuth2AccessTokenFactory",
                        oAuth2AccessTokenFactory
                )
                        .isNotNull()
                        .thenGetValue();


		this.spiffApiGatewayService =
                guardThat(
                        "spiffApiGatewayService",
                        spiffApiGatewayService
                )
                        .isNotNull()
                        .thenGetValue();

		this.claimSpiffWebResponseFactory =
                guardThat(
                        "claimSpiffWebResponseFactory",
                        claimSpiffWebResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerRepInfoWebResponseFactory =
                guardThat(
                        "partnerRepInfoWebResponseFactory",
                        partnerRepInfoWebResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleInvoiceWebResponseFactory =
                guardThat(
                        "partnerSaleInvoiceWebResponseFactory",
                        partnerSaleInvoiceWebResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffEntitlementWithPartnerRepInfoWebResponseFactory =
                guardThat(
                        "spiffEntitlementWithPartnerRepInfoWebResponseFactory",
                        spiffEntitlementWithPartnerRepInfoWebResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffEntitlementWithPartnerRepInfoWebRequestFactory =
                guardThat(
                        "spiffEntitlementWithPartnerRepInfoWebRequestFactory",
                        spiffEntitlementWithPartnerRepInfoWebRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

		this.uploadPartnerSaleInvoiceReqWebRequestFactory =
                guardThat(
                        "uploadPartnerSaleInvoiceReqWebRequestFactory",
                        uploadPartnerSaleInvoiceReqWebRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();
	}

	@RequestMapping(value="/partnerrepinfo/{accountId}", method = RequestMethod.GET)
    @ApiOperation(value = "get the partner reps info with accountid")
	Collection<PartnerRepInfoWebView> listPartnerRepsInfoWithAccountId(
			@PathVariable("accountId") String partnerAccountId,
			@RequestHeader("Authorization") String authorizationHeader
			)throws AuthenticationException, AuthorizationException{


    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);


		return
				spiffApiGatewayService
						.listPartnerRepsInfoWithAccountId(
													new AccountIdImpl(partnerAccountId),
													accessToken
													)
													.stream()
													.map(partnerRepInfoWebResponseFactory::construct)
													.collect(Collectors.toList());


	}

	@RequestMapping(value="/spiffentitlements/{accountId}", method = RequestMethod.GET)
    @ApiOperation(value = "get the spiff entitlements and partner rep info with accountid")
	Collection<SpiffEntitlementWithPartnerRepInfoWebView> listSpiffEntitlementsAccountId(
			@PathVariable("accountId") String partnerAccountId,
			@RequestHeader("Authorization") String authorizationHeader
			)throws AuthenticationException, AuthorizationException{

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		return
				spiffApiGatewayService
						.listSpiffEntitlementsAccountId(
													new AccountIdImpl(partnerAccountId),
													accessToken
													)
													.stream()
													.map(spiffEntitlementWithPartnerRepInfoWebResponseFactory::construct)
													.collect(Collectors.toList());

	}

	@RequestMapping(value="/uploadinvoice", consumes=MediaType.MULTIPART_FORM_DATA, method = RequestMethod.POST)
    @ApiOperation(value = "upload the invoice for partner sale registration")
	PartnerSaleInvoiceWebView uploadInvoiceForPartnerSaleRegistration(
			@RequestParam("number") final String partnerSaleInvoiceNumber,
            @RequestParam(value = "partnerSaleRegId", required = false) final Long partnerSaleRegId,
            @RequestParam("file") final MultipartFile file,
			@RequestHeader("Authorization") String authorizationHeader
			)throws AuthenticationException, AuthorizationException{

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		UploadPartnerSaleInvoiceReq uploadPartnerSaleInvoiceReq =
										uploadPartnerSaleInvoiceReqWebRequestFactory
											.construct(
													partnerSaleInvoiceNumber,
													partnerSaleRegId,
													file
													);

		PartnerSaleInvoiceView partnerSaleInvoiceView =
												spiffApiGatewayService
												.uploadInvoiceForPartnerSaleRegistration(
																						uploadPartnerSaleInvoiceReq,
																						accessToken
																						);

		return
				partnerSaleInvoiceWebResponseFactory
									.construct(
											partnerSaleInvoiceView
											);

	}

	@RequestMapping(value="/claimspiffentitlements/{accountId}", method = RequestMethod.POST)
    @ApiOperation(value = "post the cliam spiff entitlements with partner rep info")
	Collection<ClaimSpiffWebView> claimSpiffEntitlements(
			@PathVariable("accountId") String partnerAccountId,
			@RequestBody Collection<SpiffEntitlementWithPartnerRepInfoWebDto> spiffEntitlementWithPartnerRepInfoWebDtos,
			@RequestHeader("Authorization") String authorizationHeader
			) throws AuthenticationException,AuthorizationException{

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		Collection<SpiffEntitlementView> spiffEntitlementWithPartnerRepInfoViews =
																spiffEntitlementWithPartnerRepInfoWebDtos
																	.stream()
																	.map(spiffEntitlementWithPartnerRepInfoWebRequestFactory::construct)
																	.collect(Collectors.toList());

		return
				spiffApiGatewayService
						.claimSpiffEntitlements(
											new AccountIdImpl(partnerAccountId),
											spiffEntitlementWithPartnerRepInfoViews,
											accessToken
											)
											.stream()
											.map(claimSpiffWebResponseFactory::construct)
											.collect(Collectors.toList());


	}
}
