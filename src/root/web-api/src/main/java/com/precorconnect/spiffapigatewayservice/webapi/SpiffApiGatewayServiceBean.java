package com.precorconnect.spiffapigatewayservice.webapi;

import javax.inject.Singleton;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.precorconnect.spiffapigatewayservice.api.SpiffApiGatewayService;
import com.precorconnect.spiffapigatewayservice.api.SpiffApiGatewayServiceConfigFactoryImpl;
import com.precorconnect.spiffapigatewayservice.api.SpiffApiGatewayServiceImpl;

@Configuration
public class SpiffApiGatewayServiceBean {

	@Bean
    @Singleton
	public SpiffApiGatewayService SpiffApiGatewayService(){

		return
				new SpiffApiGatewayServiceImpl(
						new SpiffApiGatewayServiceConfigFactoryImpl()
															.construct()
						);

	}
}
