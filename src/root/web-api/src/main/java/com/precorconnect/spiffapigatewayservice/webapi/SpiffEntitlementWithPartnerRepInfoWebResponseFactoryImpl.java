package com.precorconnect.spiffapigatewayservice.webapi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.SpiffEntitlementWithPartnerRepInfoWebView;

@Component
public class SpiffEntitlementWithPartnerRepInfoWebResponseFactoryImpl implements
		SpiffEntitlementWithPartnerRepInfoWebResponseFactory {

	@Override
	public SpiffEntitlementWithPartnerRepInfoWebView construct(
			@NonNull SpiffEntitlementWithPartnerRepInfoView spiffEntitlementWithPartnerRepInfoView
			) {

		Long spiffEntitlementId =
					spiffEntitlementWithPartnerRepInfoView
							.getSpiffEntitlementId()
							.getValue();

		Long partnerSaleRegistrationId =
				spiffEntitlementWithPartnerRepInfoView
						.getPartnerSaleRegistrationId()
						.getValue();

		DateFormat dateFormat_install = new SimpleDateFormat("MM/dd/yyyy");

		String installDate =
				dateFormat_install.format(
						spiffEntitlementWithPartnerRepInfoView
											.getInstallDate()
											.getValue()
										);

		Double spiffAmount =
				spiffEntitlementWithPartnerRepInfoView
						.getSpiffAmount()
						.getValue();

		String partnerRepUserId =
							spiffEntitlementWithPartnerRepInfoView
											.getPartnerRepInfoView()
											.getUserId()
											.getValue();

		String firstName =
						spiffEntitlementWithPartnerRepInfoView
						.getPartnerRepInfoView()
						.getFirstName();

		String lastName =
						spiffEntitlementWithPartnerRepInfoView
						.getPartnerRepInfoView()
						.getLastName();

		boolean isBankInfoExists =
								spiffEntitlementWithPartnerRepInfoView
								.getPartnerRepInfoView()
								.isBankInfoExists();

		boolean isW9InfoExists =
								spiffEntitlementWithPartnerRepInfoView
								.getPartnerRepInfoView()
								.isW9InfoExists();

		boolean isContactInfoExists =
									spiffEntitlementWithPartnerRepInfoView
									.getPartnerRepInfoView()
									.isContactInfoExists();

		String invoiceUrl =
				spiffEntitlementWithPartnerRepInfoView
						.getInvoiceInfo()
						.getInvoiceUrl()
						.getValue();

		String invoiceNumber =
				spiffEntitlementWithPartnerRepInfoView
						.getInvoiceInfo()
						.getInvoiceNumber()
						.getValue();

		String facilityName =
				spiffEntitlementWithPartnerRepInfoView
						.getFacilityName()
						.getValue();

		return
				new SpiffEntitlementWithPartnerRepInfoWebView(
						spiffEntitlementId,
						partnerSaleRegistrationId,
						installDate,
						spiffAmount,
						partnerRepUserId,
						firstName,
						lastName,
						isBankInfoExists,
						isW9InfoExists,
						isContactInfoExists,
						invoiceUrl,
						invoiceNumber,
						facilityName
						);
	}

}
