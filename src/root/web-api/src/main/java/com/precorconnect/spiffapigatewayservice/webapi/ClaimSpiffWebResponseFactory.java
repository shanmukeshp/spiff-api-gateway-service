package com.precorconnect.spiffapigatewayservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.ClaimSpiffWebView;

public interface ClaimSpiffWebResponseFactory {

	ClaimSpiffWebView construct(
						@NonNull ClaimSpiffView claimSpiffView
						);

}
