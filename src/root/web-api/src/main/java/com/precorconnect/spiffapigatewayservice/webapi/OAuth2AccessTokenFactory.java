package com.precorconnect.spiffapigatewayservice.webapi;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import org.checkerframework.checker.nullness.qual.NonNull;

interface OAuth2AccessTokenFactory {

    OAuth2AccessToken construct(
            @NonNull String authorizationHeaderValue
    ) throws AuthenticationException;

}
