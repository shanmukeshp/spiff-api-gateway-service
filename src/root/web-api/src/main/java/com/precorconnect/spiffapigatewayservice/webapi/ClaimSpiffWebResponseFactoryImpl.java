package com.precorconnect.spiffapigatewayservice.webapi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.ClaimSpiffWebView;

@Component
public class ClaimSpiffWebResponseFactoryImpl implements
		ClaimSpiffWebResponseFactory {

	@Override
	public ClaimSpiffWebView construct(
			@NonNull ClaimSpiffView claimSpiffView
			) {

		Long claimSpiffId =
							claimSpiffView
								.getClaimId()
								.getvalue();

		Long partnerSaleRegistrationId =
										claimSpiffView
											.getPartnerSaleRegistrationId()
											.getValue();
		String accountId =
						claimSpiffView
								.getPartnerAccountId()
								.getValue();

		String userId =
					claimSpiffView
							.getPartnerRepUserId()
							.getValue();

		String firstName =
				claimSpiffView
						.getPartnerRepFirstName()
						.getValue();

		String lastName =
				claimSpiffView
						.getPartnerRepLastName()
						.getValue();

		DateFormat dateFormat_install = new SimpleDateFormat("MM/dd/yyyy");

		String installDate =
				dateFormat_install.format(
										claimSpiffView
											.getInstallDate()
											.getValue()
										);

		Double spiffAmount =
						claimSpiffView
								.getSpiffAmount()
								.getValue();

		DateFormat dateFormat_claim = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		String spiffClaimedDate =
							dateFormat_claim.format(
													claimSpiffView
														.getSpiffClaimedDate()
														.getValue()
													);

		String facilityName =
						claimSpiffView
								.getFacilityName()
								.getValue();

		String invoiceNumber =
						claimSpiffView
								.getInvoiceNumber()
								.getValue();

		String firstName = claimSpiffView.getPartnerRepFirstName().getValue();

		String lastName = claimSpiffView.getPartnerRepLastName().getValue();

		return
				new ClaimSpiffWebView(
						claimSpiffId,
						partnerSaleRegistrationId,
						accountId,
						userId,
						firstName,
						lastName,
						installDate,
						spiffAmount,
						spiffClaimedDate,
						facilityName,
						invoiceNumber
						);
	}

}
