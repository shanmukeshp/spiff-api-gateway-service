package com.precorconnect.spiffapigatewayservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.SpiffEntitlementWithPartnerRepInfoWebDto;

public interface SpiffEntitlementWithPartnerRepInfoWebRequestFactory {

	SpiffEntitlementView construct(
			@NonNull SpiffEntitlementWithPartnerRepInfoWebDto spiffEntitlementWithPartnerRepInfoWebDto
			);

}
