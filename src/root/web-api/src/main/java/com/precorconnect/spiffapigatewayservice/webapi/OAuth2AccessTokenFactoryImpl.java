package com.precorconnect.spiffapigatewayservice.webapi;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

@Component
class OAuth2AccessTokenFactoryImpl
        implements OAuth2AccessTokenFactory {

    @Override
    public OAuth2AccessToken construct(
            @NonNull String authorizationHeaderValue
    ) throws AuthenticationException {

        // get access token
        OAuth2AccessToken accessToken;
        try {

            String accessTokenValue =
                    authorizationHeaderValue.replaceFirst("[bB]earer", "").trim();

            return new OAuth2AccessTokenImpl(accessTokenValue);

        } catch (Exception e) {

            throw new AuthenticationException();

        }

    }
}
