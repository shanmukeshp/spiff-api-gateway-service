package com.precorconnect.spiffapigatewayservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.PartnerSaleInvoiceWebView;

@Component
public class PartnerSaleInvoiceWebResponseFactoryImpl implements
		PartnerSaleInvoiceWebResponseFactory {

	@Override
	public PartnerSaleInvoiceWebView construct(
			@NonNull PartnerSaleInvoiceView partnerSaleInvoiceView) {

		String partnerSaleInvoiceId =
						partnerSaleInvoiceView
							.getId()
							.getValue();

		String partnerSaleInvoiceNumber =
							partnerSaleInvoiceView
								.getNumber()
								.getValue();
		String fileUrl =
					partnerSaleInvoiceView
						.getFileUrl()
						.toString();

		Long partnerSaleRegId =
					partnerSaleInvoiceView
						.getPartnerSaleRegId()
						.getValue();

		return
				new PartnerSaleInvoiceWebView(
								partnerSaleInvoiceId,
								partnerSaleInvoiceNumber,
								fileUrl,
								partnerSaleRegId
								);
	}

}
