package com.precorconnect.spiffapigatewayservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.PartnerRepInfoWebView;

@Component
public class PartnerRepInfoWebResponseFactoryImpl implements
		PartnerRepInfoWebResponseFactory {

	@Override
	public PartnerRepInfoWebView construct(
			@NonNull PartnerRepInfoView partnerRepInfoView
			) {

		String userId =
					partnerRepInfoView
						.getUserId()
						.getValue();

		String firstName =
					partnerRepInfoView
						.getFirstName();

		String lastName =
					partnerRepInfoView
						.getLastName();

		boolean isBankInfoExists =
						partnerRepInfoView
							.isBankInfoExists();

		boolean isW9InfoExists =
						partnerRepInfoView
						.isW9InfoExists();

		boolean isContactInfoExists =
						partnerRepInfoView
						.isContactInfoExists();

		return
				new PartnerRepInfoWebView(
						userId,
						firstName,
						lastName,
						isBankInfoExists,
						isW9InfoExists,
						isContactInfoExists
						);
	}

}
