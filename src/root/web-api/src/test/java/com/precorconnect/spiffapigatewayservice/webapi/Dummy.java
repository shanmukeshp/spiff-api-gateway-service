package com.precorconnect.spiffapigatewayservice.webapi;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;

import com.precorconnect.spiffgatewayservice.webapiobjectmodel.PartnerRepInfoWebDto;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.SpiffEntitlementWithPartnerRepInfoWebDto;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.UploadPartnerSaleInvoiceReqWebDto;

public class Dummy {

    /*
    fields
     */
    private URI uri;

    private String accountId;

    private UploadPartnerSaleInvoiceReqWebDto uploadPartnerSaleInvoiceReqWebDto;

	private Long partnerSaleRegId = 1234L;

    private String partnerSaleInvoiceNumber = "75313424563432411123";

    String sampleFileContent = "hello world";

    private byte[] fileContent = sampleFileContent.getBytes();

    private String fileExtension = "txt";

    private File file ;

    Collection<SpiffEntitlementWithPartnerRepInfoWebDto>  SpiffEntitlementWithPartnerRepInfoWebDtos = new ArrayList<SpiffEntitlementWithPartnerRepInfoWebDto>();

    SpiffEntitlementWithPartnerRepInfoWebDto spiffEntitlementWithPartnerRepInfoWebDto;

	private Long spiffEntitlementId = 25L;

	private Long partnerSaleRegistrationId = 1234L;

	private String installDate = "11/21/1985 02:23:00";

	private Double spiffAmount = 100.00;

	private PartnerRepInfoWebDto partnerRepInfoWebDto;

	private String invoiceUrl = "www.test.com";

	private String invoiceNumber = "11009933";

	private String facilityName = "precorfacility";

	private String userId = "9846383";

	private String firstName = "myfname";

	private String lastName = "mylname";

	private boolean isBankInfoExists = true;

	private boolean isW9InfoExists = true;

	private boolean isContactInfoExists = true;


    /*
    constructors
     */
    public Dummy() {

        try {

            uri = new URI("http://dev.precorconnect.com");

            accountId = "123494863884523475";

            file = new File("src/test/resources/test-file.png");

            uploadPartnerSaleInvoiceReqWebDto =
            					new UploadPartnerSaleInvoiceReqWebDto(
            													partnerSaleRegId,
            													partnerSaleInvoiceNumber,
            													file
            													);

            partnerRepInfoWebDto =
            			new PartnerRepInfoWebDto(
            									userId,
            									firstName,
            									lastName,
            									isBankInfoExists,
            									isW9InfoExists,
            									isContactInfoExists
            									);

            spiffEntitlementWithPartnerRepInfoWebDto =
            				new SpiffEntitlementWithPartnerRepInfoWebDto(
            												spiffEntitlementId,
            												partnerSaleRegistrationId,
            												installDate,
            												spiffAmount,
            												userId,
            												firstName,
            												lastName,
            												invoiceUrl,
            												invoiceNumber,
            												facilityName
            												);

            SpiffEntitlementWithPartnerRepInfoWebDtos
            					.add(spiffEntitlementWithPartnerRepInfoWebDto);


        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

    }

	/*
    getter methods
     */
    public URI getUri() {

        return uri;

    }

	public String getAccountId() {
		return accountId;
	}

    public UploadPartnerSaleInvoiceReqWebDto getUploadPartnerSaleInvoiceReqWebDto() {
		return uploadPartnerSaleInvoiceReqWebDto;
	}

	public Long getPartnerSaleRegId() {
		return partnerSaleRegId;
	}

	public String getPartnerSaleInvoiceNumber() {
		return partnerSaleInvoiceNumber;
	}

	public String getSampleFileContent() {
		return sampleFileContent;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public Collection<SpiffEntitlementWithPartnerRepInfoWebDto> getSpiffEntitlementWithPartnerRepInfoWebDtos() {
		return SpiffEntitlementWithPartnerRepInfoWebDtos;
	}

	public SpiffEntitlementWithPartnerRepInfoWebDto getSpiffEntitlementWithPartnerRepInfoWebDto() {
		return spiffEntitlementWithPartnerRepInfoWebDto;
	}

	public Long getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public PartnerRepInfoWebDto getPartnerRepInfoWebDto() {
		return partnerRepInfoWebDto;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getUserId() {
		return userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public boolean isBankInfoExists() {
		return isBankInfoExists;
	}

	public boolean isW9InfoExists() {
		return isW9InfoExists;
	}

	public boolean isContactInfoExists() {
		return isContactInfoExists;
	}


}
