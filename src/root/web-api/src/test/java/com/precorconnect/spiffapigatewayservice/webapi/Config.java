package com.precorconnect.spiffapigatewayservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.identityservice.HmacKey;


public class Config {

    /*
    fields
     */
    private final HmacKey identityServiceJwtSigningKey;

    private final String baseUrl;

    /*
    constructors
    */
    public Config(
    		@NonNull final HmacKey identityServiceJwtSigningKey,
    		@NonNull final String baseUrl
    		) {

    	this.identityServiceJwtSigningKey = identityServiceJwtSigningKey;

    	this.baseUrl = baseUrl;
    }

    /*
    getter methods
     */
    public HmacKey getIdentityServiceJwtSigningKey() {
        return identityServiceJwtSigningKey;
    }

	public String getBaseUrl() {
		return baseUrl;
	}



}
