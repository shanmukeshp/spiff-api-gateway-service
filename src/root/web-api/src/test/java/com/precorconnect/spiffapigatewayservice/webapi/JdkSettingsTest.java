package com.precorconnect.spiffapigatewayservice.webapi;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Basic sanity check test to ensure that build environment actually enables
 * storage of parameter names
 */
public class JdkSettingsTest {
    static class Bean {
        public Bean(String beanName1) {
        }

        public static void factory(String name, Integer foobar) {
        }
    }

    @Test
    public void testMethodParameterNames() throws Exception {
        Constructor<?> ctor = Bean.class.getDeclaredConstructor(String.class);
        assertThat(ctor)
                .isNotNull();
        assertThat(ctor.getParameters()[0].getName())
                .isEqualTo("beanName1");
        Method m = Bean.class.getDeclaredMethod("factory", String.class, Integer.class);
        assertThat(m.getParameters()[1].getName())
                .isEqualTo("foobar");
    }
}
