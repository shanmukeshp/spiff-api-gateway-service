package org.spiff.entitlement.service.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl;

public interface UpdateInvoiceUrlFeature {

	void execute(@NonNull PartnerSaleRegId partnerSaleRegistrationId, @NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException;

}
