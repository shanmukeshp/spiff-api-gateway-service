package org.spiff.entitlement.service.adapter;

import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastNameImpl;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.PartnerSaleRegIdImpl;
import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityName;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDateImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceInfoImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementViewImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;

public final class SpiffEntitlementViewFactoryImpl implements SpiffEntitlementViewFactory {

	@Override
	public SpiffEntitlementView construct(
			com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView spiffEntitlementView) {
		return new SpiffEntitlementViewImpl(convert(spiffEntitlementView.getPartnerSaleRegistrationId()),
											convert(spiffEntitlementView.getInstallDate()),
											convert(spiffEntitlementView.getSpiffAmount()),
											new UserIdImpl(spiffEntitlementView.getPartnerRepUserId().getValue()),
											new FirstNameImpl(spiffEntitlementView.getFirstName().getValue()),
											new LastNameImpl(spiffEntitlementView.getLastName().getValue()),
											convert(spiffEntitlementView.getInvoiceUrl(), spiffEntitlementView.getInvoiceNumber()),
											convert(spiffEntitlementView.getFacilityName()),
											convert(spiffEntitlementView.getSpiffEntitlementId()));
	}

	private PartnerSaleRegId convert(final PartnerSaleRegistrationId partnerSaleRegId) {
		return new PartnerSaleRegIdImpl(partnerSaleRegId.getValue());
	}

	private InstallDate convert(final com.precorconnect.spiffentitlementservice.objectmodel.InstallDate installDate) {
		return new InstallDateImpl(installDate.getValue());

	}

	private SpiffAmount convert(final com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmount spiffAmount) {
		return new SpiffAmountImpl(spiffAmount.getValue());

	}


	private InvoiceInfo convert(final com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl invoiceUrl,
			final com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumber invoiceNumber) {
		return new InvoiceInfoImpl(new InvoiceUrlImpl(invoiceUrl.getValue()), new InvoiceNumberImpl(invoiceNumber.getValue()));
	}

	private FacilityName convert(final com.precorconnect.spiffentitlementservice.objectmodel.FacilityName facilityName) {
		return new FacilityNameImpl(facilityName.getValue());

	}

	private SpiffEntitlementId convert(final com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId spiffEntitlementId) {
		return new SpiffEntitlementIdImpl(spiffEntitlementId.getValue());

	}


}
