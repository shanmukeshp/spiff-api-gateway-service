package org.spiff.entitlement.service.adapter;

import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementId;

public interface DeleteSpiffEntitlementsFeature {

	void execute(
			@NonNull List<SpiffEntitlementId> spiffEntitlementIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

}
