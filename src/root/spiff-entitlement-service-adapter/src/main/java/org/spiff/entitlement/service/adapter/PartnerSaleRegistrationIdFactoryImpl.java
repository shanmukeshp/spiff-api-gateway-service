package org.spiff.entitlement.service.adapter;

import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationIdImpl;

public final class PartnerSaleRegistrationIdFactoryImpl implements PartnerSaleRegistrationIdFactory {

	@Override
	public PartnerSaleRegistrationId construct(
			PartnerSaleRegId partnerSaleRegid) {

		return new PartnerSaleRegistrationIdImpl(partnerSaleRegid.getValue());
	}

}
