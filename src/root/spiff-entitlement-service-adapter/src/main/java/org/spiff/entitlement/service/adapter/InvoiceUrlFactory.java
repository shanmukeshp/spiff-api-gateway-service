package org.spiff.entitlement.service.adapter;

import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;

public interface InvoiceUrlFactory {

	InvoiceUrl construct(com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl invoiceUrl);
}
