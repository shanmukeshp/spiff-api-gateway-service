package org.spiff.entitlement.service.adapter;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.spiffapigatewayservice.core.SpiffEntitlementServiceAdapter;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;

public final class SpiffEntitlementServiceAdapterImpl implements SpiffEntitlementServiceAdapter {

	 /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public SpiffEntitlementServiceAdapterImpl(
            @NonNull SpiffEntitlementServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }
	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(@NonNull AccountId partnerAccountId,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {
        return injector
        .getInstance(ListEntitlementsWithPartnerIdFeature.class)
        .execute(partnerAccountId, accessToken);
	}

	@Override
	public void updateInvoiceUrl(@NonNull PartnerSaleRegId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl, @NonNull OAuth2AccessToken accessToken) throws AuthenticationException , AuthorizationException{
         injector
        .getInstance(UpdateInvoiceUrlFeature.class)
        .execute(partnerSaleRegistrationId, invoiceUrl, accessToken);

	}
	@Override
	public void deleteSpiffEntitlements(@NonNull List<SpiffEntitlementId> spiffEntitlementIds,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException , AuthorizationException{
        injector
       .getInstance(DeleteSpiffEntitlementsFeature.class)
       .execute(spiffEntitlementIds, accessToken);

	}

}
