package org.spiff.entitlement.service.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import com.precorconnect.spiffentitlementservice.sdk.SpiffEntitlementServiceSdkConfig;

public final class SpiffEntitlementServiceAdapterConfigImpl implements SpiffEntitlementServiceAdapterConfig {
	
	private SpiffEntitlementServiceSdkConfig spiffEntitlementServiceSdkConfig;
	
	public SpiffEntitlementServiceAdapterConfigImpl(final SpiffEntitlementServiceSdkConfig spiffEntitlementServiceSdkConfig) {
		this.spiffEntitlementServiceSdkConfig = guardThat(
                "spiffEntitlementServiceSdkConfig",
                spiffEntitlementServiceSdkConfig
			  ).isNotNull()
			   .thenGetValue();;
	}

	@Override
	public SpiffEntitlementServiceSdkConfig getSpiffEntitlementServiceSdkConfig() {
		return spiffEntitlementServiceSdkConfig;
	}

}
