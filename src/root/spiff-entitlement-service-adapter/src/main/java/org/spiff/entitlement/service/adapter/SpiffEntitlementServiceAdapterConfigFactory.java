package org.spiff.entitlement.service.adapter;

public interface SpiffEntitlementServiceAdapterConfigFactory {

	SpiffEntitlementServiceAdapterConfig construct();
}
