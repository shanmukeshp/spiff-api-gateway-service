package org.spiff.entitlement.service.adapter;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;

public interface ListEntitlementsWithPartnerIdFeature {
	Collection<SpiffEntitlementView> execute(@NonNull AccountId partnerAccountId,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException;

}
