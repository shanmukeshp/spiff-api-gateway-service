package org.spiff.entitlement.service.adapter;

import com.precorconnect.spiffentitlementservice.sdk.SpiffEntitlementServiceSdkConfig;

public interface SpiffEntitlementServiceAdapterConfig {

	SpiffEntitlementServiceSdkConfig getSpiffEntitlementServiceSdkConfig();
}
