package org.spiff.entitlement.service.adapter;

import com.precorconnect.PartnerSaleRegId;

public interface PartnerSaleRegistrationIdFactory {

	com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId construct(
			PartnerSaleRegId partnerSaleRegid);
}
