package org.spiff.entitlement.service.adapter;

import java.net.MalformedURLException;
import java.net.URL;

import com.precorconnect.spiffentitlementservice.sdk.SpiffEntitlementServiceSdkConfigImpl;

public final class SpiffEntitlementServiceAdapterConfigFactoryImpl implements SpiffEntitlementServiceAdapterConfigFactory {

	@Override
	public SpiffEntitlementServiceAdapterConfig construct() {
		return new SpiffEntitlementServiceAdapterConfigImpl(
				new SpiffEntitlementServiceSdkConfigImpl(constructPrecorConnectApiBaseUrl()));
	}

	
	 private URL constructPrecorConnectApiBaseUrl() {

	        String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");

	        try {

	            return new URL(precorConnectApiBaseUrl);

	        } catch (MalformedURLException e) {

	            throw new RuntimeException(e);

	        }
	 }
}
