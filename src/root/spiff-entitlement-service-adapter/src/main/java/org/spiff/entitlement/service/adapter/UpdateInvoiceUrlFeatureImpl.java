package org.spiff.entitlement.service.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.sdk.SpiffEntitlementServiceSdk;

public final class UpdateInvoiceUrlFeatureImpl implements UpdateInvoiceUrlFeature {

	private SpiffEntitlementServiceSdk spiffEntitlementsServiceSdk;

	private PartnerSaleRegistrationIdFactory partnerSaleRegistrationIdFactory;

	private InvoiceUrlFactory invoiceUrlFactory;

	@Inject
	public UpdateInvoiceUrlFeatureImpl(final SpiffEntitlementServiceSdk spiffEntitlementsServiceSdk,
									   final PartnerSaleRegistrationIdFactory factory,
									   final InvoiceUrlFactory invoiceUrlFactory) {
		this.spiffEntitlementsServiceSdk = guardThat(
                "spiffEntitlementsServiceSdk",
                spiffEntitlementsServiceSdk
			  ).isNotNull()
			   .thenGetValue();

		this.partnerSaleRegistrationIdFactory = guardThat(
                "partnerSaleRegistrationIdFactory",
                factory
			  ).isNotNull()
			   .thenGetValue();

		this.invoiceUrlFactory = guardThat(
                "invoiceUrlFactory",
                invoiceUrlFactory
			  ).isNotNull()
			   .thenGetValue();

	}

	@Override
	public void execute(@NonNull PartnerSaleRegId partnerSaleRegistrationId, @NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException , AuthorizationException{


		spiffEntitlementsServiceSdk.updateInvoiceUrl(partnerSaleRegistrationIdFactory.construct(partnerSaleRegistrationId),
													 invoiceUrlFactory.construct(invoiceUrl),
													 accessToken);

	}

}
