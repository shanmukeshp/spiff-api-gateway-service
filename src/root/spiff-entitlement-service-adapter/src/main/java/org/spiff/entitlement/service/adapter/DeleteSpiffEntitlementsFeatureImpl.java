package org.spiff.entitlement.service.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.List;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.sdk.SpiffEntitlementServiceSdk;

public final class DeleteSpiffEntitlementsFeatureImpl implements DeleteSpiffEntitlementsFeature {

	private SpiffEntitlementServiceSdk spiffEntitlementsServiceSdk;


	@Inject
	public DeleteSpiffEntitlementsFeatureImpl(final SpiffEntitlementServiceSdk spiffEntitlementsServiceSdk,
									   final SpiffEntitlementViewFactory spiffEntitlementViewFactory) {
		this.spiffEntitlementsServiceSdk = guardThat(
                "spiffEntitlementsServiceSdk",
                spiffEntitlementsServiceSdk
			  ).isNotNull()
			   .thenGetValue();

	}


	@Override
	public void execute(
			@NonNull List<SpiffEntitlementId> spiffEntitlementIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException , AuthorizationException{

		List<com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId> convertedIds = spiffEntitlementIds
				.stream()
				.map(id -> new com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementIdImpl(id.getValue()))
				.collect(Collectors.toList());

		spiffEntitlementsServiceSdk.deleteSpiffEntitlements(convertedIds, accessToken);
	}




}
