package org.spiff.entitlement.service.adapter;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public interface SpiffEntitlementViewFactory {

	com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView construct(
			SpiffEntitlementView spiffEntitlementView);
}
