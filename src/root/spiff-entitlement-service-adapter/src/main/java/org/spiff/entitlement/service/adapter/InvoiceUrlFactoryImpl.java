package org.spiff.entitlement.service.adapter;

import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;

public final class InvoiceUrlFactoryImpl implements InvoiceUrlFactory {

	@Override
	public InvoiceUrl construct(com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl invoiceUrl) {
		return new InvoiceUrlImpl(invoiceUrl.getValue());
	}

}
