package org.spiff.entitlement.service.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffentitlementservice.sdk.SpiffEntitlementServiceSdk;

public final class ListEntitlementsWithPartnerIdFeatureImpl implements ListEntitlementsWithPartnerIdFeature {

	private SpiffEntitlementServiceSdk spiffEntitlementsServiceSdk;

	private SpiffEntitlementViewFactory spiffEntitlementViewFactory;


	@Inject
	public ListEntitlementsWithPartnerIdFeatureImpl(final SpiffEntitlementServiceSdk spiffEntitlementsServiceSdk,
									   final SpiffEntitlementViewFactory spiffEntitlementViewFactory) {
		this.spiffEntitlementsServiceSdk = guardThat(
                "spiffEntitlementsServiceSdk",
                spiffEntitlementsServiceSdk
			  ).isNotNull()
			   .thenGetValue();

		this.spiffEntitlementViewFactory = guardThat(
                "spiffEntitlementViewFactory",
                spiffEntitlementViewFactory
			  ).isNotNull()
			   .thenGetValue();


	}


	@Override
	public Collection<SpiffEntitlementView> execute(@NonNull AccountId partnerAccountId,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {
		Collection<SpiffEntitlementView> spiffEntitlements = new ArrayList<SpiffEntitlementView>();

		spiffEntitlements.addAll(spiffEntitlementsServiceSdk.listEntitlementsWithPartnerId(partnerAccountId, accessToken)
								   .stream()
								   .map(spiffEntitlementViewFactory::construct)
								   .collect(Collectors.toList()));

		return spiffEntitlements;
	}

}
