package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import java.sql.Timestamp;

import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.PartnerSaleRegIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisView;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityName;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDateImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceNumber;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffClaimedDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffClaimedDateImpl;

public final class ClaimSpiffViewFactoryImpl implements ClaimSpiffViewFactory {

	@Override
	public ClaimSpiffSynopysisView construct(com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView claimSpiffView) {
		return new ClaimSpiffSynopysisViewImpl(
				convert(claimSpiffView.getClaimId()),
				convert(claimSpiffView.getPartnerSaleRegistrationId()),
				claimSpiffView.getPartnerAccountId(),
				claimSpiffView.getPartnerRepUserId(),
				claimSpiffView.getFirstName(),
				claimSpiffView.getLastName(),
				convert(claimSpiffView.getInstallDate()),
				convert(claimSpiffView.getSpiffAmount()),
				convert(claimSpiffView.getSpiffClaimedDate()),
				convert(claimSpiffView.getFacilityName()),
				convert(claimSpiffView.getInvoiceNumber()));

	}

	private ClaimSpiffId convert(final com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId claimSpiffId) {
		return new ClaimSpiffIdImpl(claimSpiffId.getValue());
	}

	private PartnerSaleRegId convert(final com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId partnerSaleRegid) {
		return new PartnerSaleRegIdImpl(partnerSaleRegid.getValue());
	}

	private InstallDate convert(final com.precorconnect.claimspiffservice.objectmodel.InstallDate installDate) {
		return new InstallDateImpl(installDate.getValue());
	}


	private SpiffAmount convert(final com.precorconnect.claimspiffservice.objectmodel.SpiffAmount spiffAmount) {
		return new SpiffAmountImpl(spiffAmount.getValue());
	}


	private SpiffClaimedDate convert(final com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDate spiffClaimedDate) {
		return new SpiffClaimedDateImpl(new Timestamp(spiffClaimedDate.getValue().getTime()));
	}

	private FacilityName convert(final com.precorconnect.claimspiffservice.objectmodel.FacilityName facilityName) {
		return new FacilityNameImpl(facilityName.getValue());
	}

	private InvoiceNumber convert(final com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber invoiceNumber) {
		return new InvoiceNumberImpl(invoiceNumber.getValue());
	}

}
