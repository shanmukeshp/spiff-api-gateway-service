package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import java.net.MalformedURLException;
import java.net.URL;

import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdkConfigImpl;

public final class ClaimSpiffsServiceAdapterConfigFactoryImpl implements ClaimSpiffsServiceAdapterConfigFactory {


	@Override
	public ClaimSpiffsServiceAdapterConfig construct() {
		return new ClaimSpiffsServiceAdapterConfigImpl(new ClaimSpiffServiceSdkConfigImpl(constructPrecorConnectApiBaseUrl()));
	}

	 private URL constructPrecorConnectApiBaseUrl() {

	        //String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");
		 String precorConnectApiBaseUrl = "http://172.16.101.78:8080";

	        try {

	            return new URL(precorConnectApiBaseUrl);

	        } catch (MalformedURLException e) {

	            throw new RuntimeException(e);

	        }
	 }

}
