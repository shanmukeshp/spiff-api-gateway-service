package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdk;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityName;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceNumber;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffClaimedDate;

@Singleton
public final class AddClaimSpiffsFeatureImpl implements AddClaimSpiffsFeature {

	private final ClaimSpiffServiceSdk claimSpiffServiceSdk;

	@Inject
	public AddClaimSpiffsFeatureImpl(final ClaimSpiffServiceSdk claimSpiffServiceSdk) {
		this.claimSpiffServiceSdk = guardThat(
                "claimSpiffServiceSdk",
                claimSpiffServiceSdk
			  ).isNotNull()
			   .thenGetValue();;

	}

	@Override
	public Collection<ClaimSpiffId> execute(@NonNull Collection<ClaimSpiffDto> claimSpiffDtos,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException {

		Collection<com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto> tranformedClaimSpiffDtos = claimSpiffDtos
				.stream()
				.map(claimSpiffDto -> new com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDtoImpl(
						convert(claimSpiffDto.getPartnerSaleRegistrationId()),
						claimSpiffDto.getPartnerAccountId(), claimSpiffDto
								.getPartnerRepUserId(), convert(claimSpiffDto
								.getInstallDate()), convert(claimSpiffDto
								.getSpiffAmount()), convert(claimSpiffDto
								.getSpiffClaimedDate()), convert(claimSpiffDto
								.getFacilityName()), convert(claimSpiffDto
								.getInvoiceNumber())))
				.collect(Collectors.toList());

		return claimSpiffServiceSdk.addCliamSpiffs(tranformedClaimSpiffDtos, accessToken)
			   .stream()
			   .map(claimSpiffId -> new ClaimSpiffIdImpl(claimSpiffId.getValue()))
			   .collect(Collectors.toList());


	}

	private com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId convert(final PartnerSaleRegId partnerSaleRegid) {
		return new com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl(partnerSaleRegid.getValue());
	}

	private com.precorconnect.claimspiffservice.objectmodel.InstallDate convert(final InstallDate installDate) {
		return new com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl(installDate.getValue());
	}


	private com.precorconnect.claimspiffservice.objectmodel.SpiffAmount convert(final SpiffAmount spiffAmount) {
		return new com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl(spiffAmount.getValue());
	}


	private com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDate convert(final SpiffClaimedDate spiffClaimedDate) {
		return new com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDateImpl(new Timestamp(spiffClaimedDate.getValue().getTime()));
	}

	private com.precorconnect.claimspiffservice.objectmodel.FacilityName convert(final FacilityName facilityName) {
		return new com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl(facilityName.getValue());
	}

	private com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber convert(final InvoiceNumber invoiceNumber) {
		return new com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl(invoiceNumber.getValue());
	}


}
