package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import com.precorconnect.partnerrepservice.PartnerRepContactInfoExists;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepContactInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepContactInfoImpl;

public final class PartnerRepContactInfoFactoryImpl implements PartnerRepContactInfoFactory {

	@Override
	public PartnerRepContactInfo construct(PartnerRepContactInfoExists partnerRepContactInfoExists) {
		return new PartnerRepContactInfoImpl(partnerRepContactInfoExists.getId(),
				partnerRepContactInfoExists.isInfoExists());
	}

}
