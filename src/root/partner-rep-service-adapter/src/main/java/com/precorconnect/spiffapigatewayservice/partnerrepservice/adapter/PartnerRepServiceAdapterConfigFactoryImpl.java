package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import java.net.MalformedURLException;
import java.net.URL;

import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdkConfigImpl;

public final class PartnerRepServiceAdapterConfigFactoryImpl implements PartnerRepServiceAdapterConfigFactory {

	@Override
	public PartnerRepServiceAdapterConfig construct() {
		return new PartnerRepServiceAdapterConfigImpl(
				new PartnerRepServiceSdkConfigImpl(
						constructPrecorConnectApiBaseUrl()));
	}

	 private URL constructPrecorConnectApiBaseUrl() {

	        String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");

	        try {

	            return new URL(precorConnectApiBaseUrl);

	        } catch (MalformedURLException e) {

	            throw new RuntimeException(e);

	        }
	 }
}
