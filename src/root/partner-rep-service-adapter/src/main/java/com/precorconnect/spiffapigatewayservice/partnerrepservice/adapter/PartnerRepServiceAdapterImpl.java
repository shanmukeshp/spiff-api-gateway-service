package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffapigatewayservice.core.PartnerRepServiceAdapter;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedBankInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedW9Info;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepContactInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisView;

public class PartnerRepServiceAdapterImpl implements
        PartnerRepServiceAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public PartnerRepServiceAdapterImpl(
            @NonNull PartnerRepServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }

	@Override
	public Collection<PartnerRepSynopsisView> getPartnerRepsWithIds(
			@NonNull Collection<UserId> partnerRepIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
        return injector
        .getInstance(GetPartnerRepsWithIdsFeature.class)
        .execute(partnerRepIds, accessToken);
	}

	@Override
	public Collection<MostRecentlyCommitedBankInfo> isBankInfoExistsForPartnerReps(
			@NonNull Collection<UserId> userIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
        return injector
        .getInstance(IsBankInfoExistsForPartnerRepsFeature.class)
        .execute(userIds, accessToken);
	}

	@Override
	public Collection<MostRecentlyCommitedW9Info> isW9InfoExistsForPartnerReps(
			@NonNull Collection<UserId> userIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
        return injector
        .getInstance(IsW9InfoExistsForPartnerRepsFeature.class)
        .execute(userIds, accessToken);
	}

	@Override
	public Collection<PartnerRepContactInfo> isContactInfoExistsForPartnerReps(
			@NonNull Collection<UserId> userIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
        return injector
        .getInstance(IsContactInfoExistsForPartnerRepsFeature.class)
        .execute(userIds, accessToken);
	}


}
