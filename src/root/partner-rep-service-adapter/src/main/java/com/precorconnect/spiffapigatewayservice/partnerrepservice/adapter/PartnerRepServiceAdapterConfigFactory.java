package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

public interface PartnerRepServiceAdapterConfigFactory {

	PartnerRepServiceAdapterConfig construct();
}
