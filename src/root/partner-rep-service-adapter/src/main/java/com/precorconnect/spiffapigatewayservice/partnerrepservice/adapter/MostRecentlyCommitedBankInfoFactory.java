package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedBankInfo;

public interface MostRecentlyCommitedBankInfoFactory {

	MostRecentlyCommitedBankInfo construct(
			com.precorconnect.partnerrepservice.MostRecentlyCommitedBankInfoDto mostRecentlyCommittedBankInfoDto);
}
