package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisViewImpl;

public final class PartnerRepSynopsisViewFactoryImpl implements PartnerRepSynopsisViewFactory {

	@Override
	public PartnerRepSynopsisView construct(com.precorconnect.partnerrepservice.PartnerRepSynopsisView  partnerRepView) {

		return new PartnerRepSynopsisViewImpl(partnerRepView.getId(), partnerRepView.getFirstName(),
				partnerRepView.getLastName());
	}

}
