package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdk;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedW9Info;

public final class IsW9InfoExistsForPartnerRepsFeatureImpl implements IsW9InfoExistsForPartnerRepsFeature {

private final PartnerRepServiceSdk partnerRepServiceSdk;
	
	private final MostRecentlyCommitedW9InfoFactory factory;

	@Inject
	public IsW9InfoExistsForPartnerRepsFeatureImpl(final PartnerRepServiceSdk partnerRepServiceSdk,
			                                final MostRecentlyCommitedW9InfoFactory factory) {
		this.partnerRepServiceSdk = guardThat(
                "partnerRepServiceSdk",
                partnerRepServiceSdk
			  ).isNotNull()
			   .thenGetValue();
		
		this.factory = guardThat(
                "mostRecentlyCommitedW9InfoFactory",
                factory
			  ).isNotNull()
			   .thenGetValue();
	}
	
	@Override
	public Collection<MostRecentlyCommitedW9Info> execute(
			@NonNull Collection<UserId> userIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
		
		Collection<MostRecentlyCommitedW9Info> mostRecentlyCommitedW9Info = new ArrayList<MostRecentlyCommitedW9Info>();
		
		mostRecentlyCommitedW9Info.addAll(partnerRepServiceSdk.isW9InfoExistsForPartnerReps(userIds, accessToken)
						    									 .stream()
						    									 .map(factory::construct)
						    									 .collect(Collectors.toList()));

		return mostRecentlyCommitedW9Info;
	}



	
}
