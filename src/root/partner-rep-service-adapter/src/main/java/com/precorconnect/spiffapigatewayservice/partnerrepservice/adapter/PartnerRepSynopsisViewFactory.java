package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisView;

public interface PartnerRepSynopsisViewFactory {

	PartnerRepSynopsisView construct(com.precorconnect.partnerrepservice.PartnerRepSynopsisView partnerRepView);
}
