package com.precorconnect.spiffapigatewayservice.core;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.mockito.Mockito.doNothing;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisView;

@RunWith(MockitoJUnitRunner.class)
public class ClaimSpiffEntitlementsFeatureImplTest {

	/*
    fields
     */
	@Mock
	private  SpiffEntitlementServiceAdapter spiffEntitlementServiceAdapter;

	@Mock
	private  ClaimSpiffsServiceAdapter claimSpiffsServiceAdapter;

	@Mock
	private  ClaimSpiffDtoFactory claimSpiffDtoFactory;

	@Mock
	private PartnerRepServiceAdapter partnerRepServiceAdapter;

	@InjectMocks
	private ClaimSpiffEntitlementsFeatureImpl claimSpiffEntitlementsFeatureImpl;

	private Dummy dummy = new Dummy();

	@Test
	public void testClaimSpiffEntilements_whenspiffEntitlementServiceAdapterAndclaimSpiffsServiceAdapterAndclaimSpiffDtoFactory_shownClaimSpiffViewList(
			) throws AuthenticationException, AuthorizationException{

		doNothing().
		   doThrow(new RuntimeException())
		   .when(spiffEntitlementServiceAdapter).deleteSpiffEntitlements(
					Matchers.anyObject(),
					Matchers.anyObject()
					);


		Mockito.when(
				claimSpiffsServiceAdapter
					.addCliamSpiffs(
							Matchers.anyObject(),
							Matchers.anyObject()
						)).thenReturn(dummy.getListClaimSpiffId());

		Mockito.when(claimSpiffsServiceAdapter
									.getClaimSpiffsWithIds(
											dummy.getListClaimSpiffId(),
											dummy.getAccessToken()
											)).thenReturn(dummy.getClaimSpiffViewList());

		Mockito.when(partnerRepServiceAdapter.getPartnerRepsWithIds(dummy.getPartnerRepUserIds(), dummy.getAccessToken()))
				.thenReturn(dummy.getPartnerRepViews());

		Collection<ClaimSpiffSynopysisView> listClaimSpiffView=
									claimSpiffsServiceAdapter
									.getClaimSpiffsWithIds(
											dummy.getListClaimSpiffId(),
											dummy.getAccessToken());

		assertThat(listClaimSpiffView.size())
		.isEqualTo(dummy.getClaimSpiffViewList().size());

	}
}
