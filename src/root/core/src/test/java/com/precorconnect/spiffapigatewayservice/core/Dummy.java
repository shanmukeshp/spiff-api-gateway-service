package com.precorconnect.spiffapigatewayservice.core;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.FileExtension;
import com.precorconnect.FileExtensionImpl;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastNameImpl;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.PartnerSaleInvoiceIdImpl;
import com.precorconnect.PartnerSaleInvoiceNumber;
import com.precorconnect.PartnerSaleInvoiceNumberImpl;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.PartnerSaleRegIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisView;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityName;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceInfoImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceNumber;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffClaimedDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffClaimedDateImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReqImpl;



public class Dummy {

	private URI uri;

	private AccountId accountId=new AccountIdImpl("123456789012345678");

	private OAuth2AccessToken accessToken=null;

	private UploadPartnerSaleInvoiceReq uploadPartnerSaleInvoiceReq;

	private Collection<SpiffEntitlementWithPartnerRepInfoView> spiffEntitlements=new ArrayList<SpiffEntitlementWithPartnerRepInfoView>();

	private SpiffEntitlementWithPartnerRepInfoView spiffEntitlementWithPartnerRepInfoView;

	private PartnerSaleRegId partnerSaleRegistrationId=new  PartnerSaleRegIdImpl(12L);

	private InstallDate installDate;

	private SpiffAmount spiffAmount=new SpiffAmountImpl(100.00);

	private  SpiffEntitlementId spiffEntitlementId=new SpiffEntitlementIdImpl(1L);

	private PartnerRepInfoView partnerRepInfoView;

	private InvoiceInfo invoiceInfo;

	private FacilityName facilityName=new FacilityNameImpl("ABC");

	private UserId userId =new UserIdImpl("123");

	private String firstName=new String("john");

	private String lastName=new String("smith");

	private boolean isBankInfoExists=true;

	private boolean isW9InfoExists=true;

	private boolean isContactInfoExists=true;

	private InvoiceUrl invoiceUrl=new InvoiceUrlImpl("www.testinvoice.com");

	private InvoiceNumber invoiceNumber=new InvoiceNumberImpl("1234");

	private ClaimSpiffId claimSpiffId=new ClaimSpiffIdImpl(12L);

	private PartnerSaleInvoiceView partnerSaleInvoiceView;

	private Collection<ClaimSpiffSynopysisView> claimSpiffViewList=new ArrayList<ClaimSpiffSynopysisView>();

	private ClaimSpiffSynopysisView claimSpiffView;

	private SpiffClaimedDate spiffClaimedDate;

	private FileExtension fileExtension=new FileExtensionImpl("PDF");

	private PartnerSaleInvoiceNumber partnerSaleInvoiceNumber=new PartnerSaleInvoiceNumberImpl("12345");

	private PartnerSaleInvoiceId partnerSaleInvoiceId=new PartnerSaleInvoiceIdImpl("123456789012345678901234567890123456");

	private Collection<PartnerRepInfoView> listPartnerRepInfoView=new ArrayList<PartnerRepInfoView>();

	private Collection<ClaimSpiffId> listClaimSpiffId=new ArrayList<ClaimSpiffId>();

	private List<UserId> partnerRepUserIds = new ArrayList<UserId>();

	private List<PartnerRepSynopsisView> partnerRepViews = new ArrayList<PartnerRepSynopsisView>();

	private PartnerSaleRegId partnerSaleRegId=new PartnerSaleRegIdImpl(12L);

	private URL fileUrl;

	private final File file;

	public Dummy() {

		try {

            uri = new URI("http://dev.precorconnect.com");

            file = new File("src/test/resources/test-file.png");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

		try{

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date;
		try {
			date = formatter.parse("11/21/1985");
		} catch (ParseException e) {
			throw new RuntimeException("Date field parse exception: ",e);
		}

		fileUrl=new URL("https://api-dev.precorconnect.com");

		installDate = new com.precorconnect.spiffapigatewayservice.objectmodel.InstallDateImpl(date);

		spiffClaimedDate= new SpiffClaimedDateImpl(date);

		partnerRepInfoView=
				new PartnerRepInfoViewImpl(
						userId,
						firstName,
						lastName,
						isBankInfoExists,
						isW9InfoExists,
						isContactInfoExists
				);

		listPartnerRepInfoView.add(partnerRepInfoView);

		invoiceInfo=
				new InvoiceInfoImpl(
						invoiceUrl,
						invoiceNumber
				);


		spiffEntitlementWithPartnerRepInfoView=
				new SpiffEntitlementWithPartnerRepInfoViewImpl(
						partnerSaleRegistrationId,
						installDate,
						spiffAmount,
						invoiceInfo,
						facilityName,
						spiffEntitlementId,
						partnerRepInfoView
					);



		spiffEntitlements.add(spiffEntitlementWithPartnerRepInfoView);

		listClaimSpiffId.add(claimSpiffId);

		claimSpiffView=
				new ClaimSpiffSynopysisViewImpl(
						claimSpiffId,
						partnerSaleRegistrationId,
						accountId,
						userId,
						new FirstNameImpl(firstName),
						new LastNameImpl(lastName),
						installDate,
						spiffAmount,
						spiffClaimedDate,
						facilityName,
						invoiceNumber
					);

		claimSpiffViewList.add(claimSpiffView);



		uploadPartnerSaleInvoiceReq=
				new UploadPartnerSaleInvoiceReqImpl(
						partnerSaleInvoiceNumber,
						partnerSaleRegId,
						file
						);

		partnerSaleInvoiceView=
				new PartnerSaleInvoiceViewImpl(
						partnerSaleInvoiceId,
						partnerSaleInvoiceNumber,
						fileUrl,
						partnerSaleRegId
					);

		partnerRepUserIds.add(new UserIdImpl("user1"));
		partnerRepUserIds.add(new UserIdImpl("user2"));

		partnerRepViews.add(new PartnerRepSynopsisViewImpl(partnerRepUserIds.get(0), new FirstNameImpl("userf1"), new LastNameImpl("userl1")));
		partnerRepViews.add(new PartnerRepSynopsisViewImpl(partnerRepUserIds.get(1), new FirstNameImpl("userf2"), new LastNameImpl("userl2")));

		}catch(MalformedURLException e){
			throw new RuntimeException(e);
		}
	}




	public AccountId getAccountId() {
		return accountId;
	}




	public OAuth2AccessToken getAccessToken() {
		return accessToken;
	}




	public UploadPartnerSaleInvoiceReq getUploadPartnerSaleInvoiceReq() {
		return uploadPartnerSaleInvoiceReq;
	}




	public Collection<SpiffEntitlementWithPartnerRepInfoView> getSpiffEntitlements() {
		return spiffEntitlements;
	}




	public SpiffEntitlementWithPartnerRepInfoView getSpiffEntitlementWithPartnerRepInfoView() {
		return spiffEntitlementWithPartnerRepInfoView;
	}




	public PartnerSaleRegId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}




	public InstallDate getInstallDate() {
		return installDate;
	}




	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}




	public PartnerRepInfoView getPartnerRepInfoView() {
		return partnerRepInfoView;
	}




	public InvoiceInfo getInvoiceInfo() {
		return invoiceInfo;
	}




	public FacilityName getFacilityName() {
		return facilityName;
	}




	public UserId getUserId() {
		return userId;
	}




	public String getFirstName() {
		return firstName;
	}




	public String getLastName() {
		return lastName;
	}




	public boolean isBankInfoExists() {
		return isBankInfoExists;
	}




	public boolean isW9InfoExists() {
		return isW9InfoExists;
	}




	public boolean isContactInfoExists() {
		return isContactInfoExists;
	}




	public InvoiceUrl getInvoiceUrl() {
		return invoiceUrl;
	}




	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}




	public FileExtension getFileExtension() {
		return fileExtension;
	}




	public PartnerSaleInvoiceView getPartnerSaleInvoiceView() {
		return partnerSaleInvoiceView;
	}




	public Collection<ClaimSpiffSynopysisView> getClaimSpiffViewList() {
		return claimSpiffViewList;
	}




	public SpiffEntitlementId getSpiffEntitlementId() {
		return spiffEntitlementId;
	}




	public ClaimSpiffId getClaimSpiffId() {
		return claimSpiffId;
	}




	public ClaimSpiffSynopysisView getClaimSpiffView() {
		return claimSpiffView;
	}




	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}




	public PartnerSaleInvoiceNumber getPartnerSaleInvoiceNumber() {
		return partnerSaleInvoiceNumber;
	}




	public PartnerSaleInvoiceId getPartnerSaleInvoiceId() {
		return partnerSaleInvoiceId;
	}



	public PartnerSaleRegId getPartnerSaleRegId() {
		return partnerSaleRegId;
	}




	public List<UserId> getPartnerRepUserIds() {
		return partnerRepUserIds;
	}




	public void setPartnerRepUserIds(List<UserId> partnerRepUserIds) {
		this.partnerRepUserIds = partnerRepUserIds;
	}




	public List<PartnerRepSynopsisView> getPartnerRepViews() {
		return partnerRepViews;
	}




	public void setPartnerRepViews(List<PartnerRepSynopsisView> partnerRepViews) {
		this.partnerRepViews = partnerRepViews;
	}




	public URL getFileUrl() {
		return fileUrl;
	}




	public URI getUri() {
		return uri;
	}




	public File getFile() {
		return file;
	}




	public Collection<PartnerRepInfoView> getListPartnerRepInfoView() {
		return listPartnerRepInfoView;
	}




	public Collection<ClaimSpiffId> getListClaimSpiffId() {
		return listClaimSpiffId;
	}








}
