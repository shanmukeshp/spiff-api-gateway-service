package com.precorconnect.spiffapigatewayservice.core;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;

@RunWith(MockitoJUnitRunner.class)
public class UploadInvoiceForPartnerSaleRegistrationFeatureImplTest {

	/**
	 * fields
	 */
	
	@Mock
	private  PartnerSaleInvoiceServiceAdapter partnerSaleInvoiceServiceAdapter;
	
	@Mock
	private  SpiffEntitlementServiceAdapter spiffEntitlementServiceAdapter;
	
	private Dummy dummy = new Dummy();
	
	@InjectMocks
	private UploadInvoiceForPartnerSaleRegistrationFeatureImpl uploadInvoiceForPartnerSaleRegistrationFeatureImpl;
	
	@Test
	public void testUploadInvoiceForPartnerSaleRegistration_whenUploadPartnerSaleInvoiceReq_shownPartnerSaleInvoiceView(
			)throws AuthenticationException,AuthorizationException{
				
		
		Mockito.when(
				partnerSaleInvoiceServiceAdapter
					.addPartnerSaleInvoice(
							Matchers.anyObject(), 
							Matchers.anyObject()
						)).thenReturn(dummy.getPartnerSaleInvoiceId());
		
		Mockito.when(partnerSaleInvoiceServiceAdapter.getPartnerSaleInvoiceWithId(
				dummy.getPartnerSaleInvoiceId(), 
				dummy.getAccessToken()
				))
				.thenReturn(dummy.getPartnerSaleInvoiceView());
		
		PartnerSaleInvoiceView partnerSaleInvoiceView=
				partnerSaleInvoiceServiceAdapter.getPartnerSaleInvoiceWithId(
						dummy.getPartnerSaleInvoiceId(),
						dummy.getAccessToken());
		
		assertEquals(
				partnerSaleInvoiceView,
				dummy.getPartnerSaleInvoiceView()
				);
	}
	
	
}
