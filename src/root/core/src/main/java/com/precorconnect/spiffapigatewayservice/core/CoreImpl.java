package com.precorconnect.spiffapigatewayservice.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public class CoreImpl implements Core {

	private final Injector injector;

	public CoreImpl(
			@NonNull final PartnerRepServiceAdapter partnerRepServiceAdapter,
			@NonNull final PartnerRepAssociationServiceAdapter partnerRepAssociationServiceAdapter,
			@NonNull final SpiffEntitlementServiceAdapter spiffEntitlementServiceAdapter,
			@NonNull final PartnerSaleInvoiceServiceAdapter partnerSaleInvoiceServiceAdapter,
			@NonNull final ClaimSpiffsServiceAdapter claimSpiffsServiceAdapter) {

		GuiceModule guiceModule = new GuiceModule(
				partnerSaleInvoiceServiceAdapter, partnerRepServiceAdapter,
				spiffEntitlementServiceAdapter,
				partnerRepAssociationServiceAdapter, claimSpiffsServiceAdapter);

		injector = Guice.createInjector(guiceModule);
	}

	@Override
	public Collection<PartnerRepInfoView> listPartnerRepsInfoWithPartnerAccountId(
			AccountId partnerAccountId, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
		return injector.getInstance(
				ListPartnerRepsInfoWithAccountIdFeature.class).execute(
				partnerAccountId, accessToken);
	}

	@Override
	public Collection<SpiffEntitlementWithPartnerRepInfoView> listSpiffEntitlementsWithPartnerAccountId(
			final AccountId partnerAccountId,
			final OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
		return injector.getInstance(
				ListSpiffEntitlementsWithPartnerAccountIdFeature.class)
				.execute(partnerAccountId, accessToken);
	}

	@Override
	public PartnerSaleInvoiceView uploadInvoiceForPartnerSaleRegistration(
			final UploadPartnerSaleInvoiceReq req,
			final OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
		return injector.getInstance(
				UploadInvoiceForPartnerSaleRegistrationFeature.class)
				.execute(
				req,
				accessToken
				);
	}

	@Override
	public Collection<ClaimSpiffView> claimSpiffEntitlements(
			AccountId partnerAccountId,
			Collection<SpiffEntitlementView> spiffEntitlements,
			OAuth2AccessToken accessToken) throws AuthenticationException,
			AuthorizationException {
		return injector.getInstance(ClaimSpiffEntitlementsFeature.class)
				.execute(partnerAccountId, spiffEntitlements, accessToken);
	}

}
