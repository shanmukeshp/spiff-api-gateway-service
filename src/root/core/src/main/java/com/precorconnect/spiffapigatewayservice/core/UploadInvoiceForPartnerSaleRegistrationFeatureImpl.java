package com.precorconnect.spiffapigatewayservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.PartnerSaleRegIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public final class UploadInvoiceForPartnerSaleRegistrationFeatureImpl implements
		UploadInvoiceForPartnerSaleRegistrationFeature {

	private final PartnerSaleInvoiceServiceAdapter partnerSaleInvoiceServiceAdapter;

	private final SpiffEntitlementServiceAdapter spiffEntitlementServiceAdapter;


	@Inject
	public UploadInvoiceForPartnerSaleRegistrationFeatureImpl(
			@NonNull final PartnerSaleInvoiceServiceAdapter partnerSaleInvoiceServiceAdapter,
			@NonNull final SpiffEntitlementServiceAdapter spiffEntitlementServiceAdapter) {

		this.partnerSaleInvoiceServiceAdapter = guardThat(
                "partnerSaleInvoiceServiceAdapter",
                partnerSaleInvoiceServiceAdapter
			  ).isNotNull()
			   .thenGetValue();

		this.spiffEntitlementServiceAdapter = guardThat(
                "spiffEntitlementServiceAdapter",
                spiffEntitlementServiceAdapter
			  ).isNotNull()
			   .thenGetValue();

	}

	@Override
	public PartnerSaleInvoiceView execute(
			@NonNull final UploadPartnerSaleInvoiceReq req,
			@NonNull final OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {

		PartnerSaleInvoiceId partnerSaleInvoiceId = partnerSaleInvoiceServiceAdapter.addPartnerSaleInvoice(
				req,
				accessToken
				);
		PartnerSaleInvoiceView partnerSaleInvoiceView = partnerSaleInvoiceServiceAdapter.getPartnerSaleInvoiceWithId(partnerSaleInvoiceId, accessToken);
		spiffEntitlementServiceAdapter.updateInvoiceUrl(
														new PartnerSaleRegIdImpl(req.getPartnerSaleRegId().getValue()),
														new InvoiceUrlImpl(partnerSaleInvoiceView.getFileUrl().toString()),
														accessToken
														);
		return partnerSaleInvoiceView;
	}

}
