package com.precorconnect.spiffapigatewayservice.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedBankInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedW9Info;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepContactInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisView;

public interface PartnerRepServiceAdapter {

    Collection<PartnerRepSynopsisView> getPartnerRepsWithIds(
            @NonNull Collection<UserId> partnerRepIds,
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException, AuthorizationException;

    Collection<MostRecentlyCommitedBankInfo> isBankInfoExistsForPartnerReps(
    		@NonNull Collection<UserId> userIds,
    		@NonNull OAuth2AccessToken accessToken
    		) throws AuthenticationException, AuthorizationException;
    
   
    Collection<MostRecentlyCommitedW9Info> isW9InfoExistsForPartnerReps(
    		@NonNull Collection<UserId> userIds,
    		@NonNull OAuth2AccessToken accessToken
    		) throws AuthenticationException, AuthorizationException;

    Collection<PartnerRepContactInfo> isContactInfoExistsForPartnerReps(
    		@NonNull Collection<UserId> userIds,
    		@NonNull OAuth2AccessToken accessToken
    		) throws AuthenticationException, AuthorizationException;

}

