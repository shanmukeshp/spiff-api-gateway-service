package com.precorconnect.spiffapigatewayservice.core;

import java.util.Collection;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;

public interface ClaimSpiffEntitlementsFeature {

	Collection<ClaimSpiffView> execute(AccountId partnerAccountId,
			Collection<SpiffEntitlementView> spiffEntitlements, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

}
