package com.precorconnect.spiffapigatewayservice.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public interface PartnerSaleInvoiceServiceAdapter {

    PartnerSaleInvoiceId addPartnerSaleInvoice(
            @NonNull UploadPartnerSaleInvoiceReq request,
            @NonNull OAuth2AccessToken oAuth2AccessToken
    ) throws AuthenticationException, AuthorizationException;

    PartnerSaleInvoiceView getPartnerSaleInvoiceWithId(
            @NonNull PartnerSaleInvoiceId partnerSaleInvoiceId,
            @NonNull OAuth2AccessToken oAuth2AccessToken
    ) throws AuthenticationException, AuthorizationException;

}
