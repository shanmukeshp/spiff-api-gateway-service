package com.precorconnect.spiffapigatewayservice.core;

import java.util.Date;

import com.precorconnect.AccountId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffDtoImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffClaimedDateImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;

public final class ClaimSpiffDtoFactoryImpl implements ClaimSpiffDtoFactory {

	@Override
	public ClaimSpiffDto construct(AccountId partnerAccountId,
			SpiffEntitlementView spiffEntitlement) {
		return new ClaimSpiffDtoImpl(spiffEntitlement.getPartnerSaleRegistrationId(),
									 partnerAccountId,
									 spiffEntitlement.getPartnerRepuserId(),
									 spiffEntitlement.getFirstName(),
									 spiffEntitlement.getLastName(),
									 spiffEntitlement.getInstallDate(),
									 spiffEntitlement.getSpiffAmount(),
									 new SpiffClaimedDateImpl(new Date()),
									 spiffEntitlement.getFacilityName(),
									 spiffEntitlement.getInvoiceInfo().getInvoiceNumber());
	}

}
