package com.precorconnect.spiffapigatewayservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisView;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffView;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;

@Singleton
public final class ClaimSpiffEntitlementsFeatureImpl implements ClaimSpiffEntitlementsFeature {

	private final SpiffEntitlementServiceAdapter spiffEntitlementServiceAdapter;

	private final ClaimSpiffsServiceAdapter claimSpiffsServiceAdapter;

	private final ClaimSpiffDtoFactory claimSpiffDtoFactory;


	@Inject
	public ClaimSpiffEntitlementsFeatureImpl(
			@NonNull final SpiffEntitlementServiceAdapter spiffEntitlementServiceAdapter,
			@NonNull final ClaimSpiffsServiceAdapter claimSpiffsServiceAdapter,
			@NonNull final ClaimSpiffDtoFactory claimSpiffDtoFactory) {

		this.spiffEntitlementServiceAdapter = guardThat(
                "spiffEntitlementServiceAdapter",
                spiffEntitlementServiceAdapter
			  ).isNotNull()
			   .thenGetValue();

		this.claimSpiffsServiceAdapter = guardThat(
                "claimSpiffsServiceAdapter",
                claimSpiffsServiceAdapter
			  ).isNotNull()
			   .thenGetValue();

		this.claimSpiffDtoFactory = guardThat(
                "claimSpiffDtoFactory",
                claimSpiffDtoFactory
			  ).isNotNull()
			   .thenGetValue();

	}

	@Override
	public Collection<ClaimSpiffView> execute(AccountId partnerAccountId,
			Collection<SpiffEntitlementView> spiffEntitlements, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

		Collection<ClaimSpiffDto> claimSpiffDtos = new ArrayList<ClaimSpiffDto>();

		for(SpiffEntitlementView entitlement : spiffEntitlements) {
			claimSpiffDtos.add(claimSpiffDtoFactory.construct(partnerAccountId, entitlement));
		}

		List<SpiffEntitlementId> entitlementIds = spiffEntitlements.stream()
																   .map(entitlement -> entitlement.getSpiffEntitlementId())
																   .collect(Collectors.toList());


		Collection<ClaimSpiffId> claimSpiffIds = claimSpiffsServiceAdapter.addCliamSpiffs(claimSpiffDtos, accessToken);

		List<ClaimSpiffSynopysisView> claimSpiffSynopsysViews = claimSpiffsServiceAdapter.getClaimSpiffsWithIds(claimSpiffIds, accessToken)
																					   .stream()
																					   .sorted((view1, view2) -> view1.getPartnerRepUserId().getValue().compareTo(view2.getPartnerRepUserId().getValue()))
																					   .collect(Collectors.toList());

		spiffEntitlementServiceAdapter.deleteSpiffEntitlements(entitlementIds, accessToken);


		Collection<ClaimSpiffView> claimSpiffViews = new ArrayList<ClaimSpiffView>();

		for(int i=0; i < claimSpiffSynopsysViews.size(); i++) {

			ClaimSpiffSynopysisView claimSpiffSynopsysView = claimSpiffSynopsysViews.get(i);

			ClaimSpiffView claimSpiffview = new ClaimSpiffViewImpl(claimSpiffSynopsysView.getClaimId(),
																   claimSpiffSynopsysView.getPartnerSaleRegistrationId(),
																   claimSpiffSynopsysView.getPartnerAccountId(),
																   claimSpiffSynopsysView.getPartnerRepUserId(),
																   claimSpiffSynopsysView.getInstallDate(),
																   claimSpiffSynopsysView.getSpiffAmount(),
																   claimSpiffSynopsysView.getSpiffClaimedDate(),
																   claimSpiffSynopsysView.getFacilityName(),
																   claimSpiffSynopsysView.getInvoiceNumber(),
																   claimSpiffSynopsysView.getFirstName(),
																   claimSpiffSynopsysView.getLastName());

			claimSpiffViews.add(claimSpiffview);
		}


		return claimSpiffViews;
	}

}
