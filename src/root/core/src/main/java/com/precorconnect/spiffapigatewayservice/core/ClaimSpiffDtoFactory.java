package com.precorconnect.spiffapigatewayservice.core;

import com.precorconnect.AccountId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;

public interface ClaimSpiffDtoFactory {

	ClaimSpiffDto construct(AccountId partnerAccountId, SpiffEntitlementView spiffEntitlement);
}
