package com.precorconnect.spiffapigatewayservice.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepAssociationSynopsisView;

public interface PartnerRepAssociationServiceAdapter {

    Collection<PartnerRepAssociationSynopsisView> listPartnerRepAssociationsWithPartnerId(
            @NonNull AccountId partnerId,
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;

}
