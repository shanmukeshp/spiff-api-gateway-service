package com.precorconnect.spiffapigatewayservice.core;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;

public interface SpiffEntitlementServiceAdapter {
	Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId partnerAccountId,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

	void updateInvoiceUrl(
			@NonNull PartnerSaleRegId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

	 void deleteSpiffEntitlements(
			 @NonNull List<SpiffEntitlementId> spiffEntitlementIds,
			 @NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException;

}
