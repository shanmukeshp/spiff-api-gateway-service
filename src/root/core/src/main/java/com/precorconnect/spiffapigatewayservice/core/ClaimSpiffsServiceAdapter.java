package com.precorconnect.spiffapigatewayservice.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisView;

public interface ClaimSpiffsServiceAdapter {

	Collection<ClaimSpiffId> addCliamSpiffs(
			@NonNull Collection<ClaimSpiffDto> claimSpiffDto,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException;

	Collection<ClaimSpiffSynopysisView> getClaimSpiffsWithIds(
    		@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds,
    		@NonNull OAuth2AccessToken accessToken
    		) throws AuthenticationException;

}
