package com.precorconnect.spiffapigatewayservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;

@Singleton
public final class ListPartnerRepsInfoWithAccountIdFeatureImpl implements ListPartnerRepsInfoWithAccountIdFeature {
		
	private final PartnerRepServiceAdapter partnerRepServiceAdapter;
	
	private final PartnerRepAssociationServiceAdapter partnerRepAssociationServiceAdapter;
	
	private final FindPartnerRepsInfoWithAccountIdHelper findPartnerRepsInfoWithAccountIdHelper;
	
	
	@Inject
	public ListPartnerRepsInfoWithAccountIdFeatureImpl(			
			@NonNull final PartnerRepServiceAdapter partnerRepServiceAdapter,
			@NonNull final PartnerRepAssociationServiceAdapter partnerRepAssociationServiceAdapter) {


		this.partnerRepServiceAdapter = guardThat(
                "partnerRepServiceAdapter",
                partnerRepServiceAdapter
			  ).isNotNull()
			   .thenGetValue();
		
		this.partnerRepAssociationServiceAdapter = guardThat(
                "partnerRepAssociationServiceAdapter",
                partnerRepAssociationServiceAdapter
			  ).isNotNull()
			   .thenGetValue();
		
		findPartnerRepsInfoWithAccountIdHelper = new FindPartnerRepsInfoWithAccountIdHelperImpl();

	}

	@Override
	public Collection<PartnerRepInfoView> execute(AccountId partnerAccountId,
												  OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {
				
		return findPartnerRepsInfoWithAccountIdHelper.execute(partnerRepServiceAdapter, 
															  partnerRepAssociationServiceAdapter, 
															  partnerAccountId, 
															  accessToken);
	}

}
