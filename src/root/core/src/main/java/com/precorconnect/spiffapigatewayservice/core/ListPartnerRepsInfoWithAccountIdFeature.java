package com.precorconnect.spiffapigatewayservice.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;

public interface ListPartnerRepsInfoWithAccountIdFeature {

	Collection<PartnerRepInfoView> execute(@NonNull AccountId partnerAccountId, 
										   @NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException;

}
