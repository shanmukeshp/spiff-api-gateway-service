package com.precorconnect.spiffapigatewayservice.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.precorconnect.AccountId;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedBankInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedW9Info;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepContactInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisView;

public final class FindPartnerRepsInfoWithAccountIdHelperImpl implements FindPartnerRepsInfoWithAccountIdHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(FindPartnerRepsInfoWithAccountIdHelperImpl.class);


	@Override
	public Collection<PartnerRepInfoView> execute(final PartnerRepServiceAdapter partnerRepServiceAdapter,
												  final PartnerRepAssociationServiceAdapter partnerRepAssociationServiceAdapter,
												  final AccountId partnerAccountId,
												  final OAuth2AccessToken accessToken) {



		try {

		LOGGER.debug(String.format("calling partner rep association service with partner Account id=%s", partnerAccountId.getValue()));

		Collection<UserId> partnerRepIds = partnerRepAssociationServiceAdapter.listPartnerRepAssociationsWithPartnerId(partnerAccountId, accessToken)
																			  .stream()
																			  .map(partnerRepAssociationSynopsisView -> partnerRepAssociationSynopsisView.getRepId())
																			  .collect(Collectors.toList());

		LOGGER.debug(String.format("%s reps associated with partner account id %s", partnerRepIds.size(), partnerAccountId.getValue()));

		return getPartnerRepInfoViews(partnerRepServiceAdapter, partnerRepIds, accessToken);

		} catch(final Exception e) {
			throw new RuntimeException(e);
		}
	}


	@Override
	public Collection<PartnerRepInfoView> execute(
			PartnerRepServiceAdapter partnerRepServiceAdapter,
			List<UserId> partnerRepIds, OAuth2AccessToken accessToken) {
		return getPartnerRepInfoViews(partnerRepServiceAdapter, partnerRepIds, accessToken);
	}


	private List<PartnerRepInfoView> getPartnerRepInfoViews(final PartnerRepServiceAdapter partnerRepServiceAdapter,
			  												final Collection<UserId> partnerRepIds,
			  												final OAuth2AccessToken accessToken) {

		List<PartnerRepInfoView> partnerRepInfos = new ArrayList<PartnerRepInfoView>();



		try {
			Collection<PartnerRepSynopsisView> partnerRepInfoView = partnerRepServiceAdapter.getPartnerRepsWithIds(partnerRepIds, accessToken);

			CompletableFuture<Collection<PartnerRepSynopsisView>>  partnerRepsViewFuture = CompletableFuture.supplyAsync(new Supplier<Collection<PartnerRepSynopsisView>>() {

			@Override
			public Collection<PartnerRepSynopsisView> get() {
					try {
						LOGGER.debug(String.format("calling getPartnerRepsWithIds on partnerRepServiceAdapter"));
							Collection<PartnerRepSynopsisView> partnerRepView = partnerRepServiceAdapter.getPartnerRepsWithIds(partnerRepIds, accessToken);
						LOGGER.debug(String.format("got %s partnerRepInfoViews", partnerRepInfoView.size()));
						return partnerRepView;
					} catch(final Exception e) {
						throw new RuntimeException("exception while retrieving partner reps information", e);
					}
			}

		});


		CompletableFuture<Collection<MostRecentlyCommitedBankInfo>>  mostRecenltyCommittedBankInfoFuture = CompletableFuture.supplyAsync(new Supplier<Collection<MostRecentlyCommitedBankInfo>>() {

			@Override
			public Collection<MostRecentlyCommitedBankInfo> get() {
				try {
					LOGGER.debug(String.format("calling bankinfos on partnerRepServiceAdapter"));

					Collection<MostRecentlyCommitedBankInfo> bankInfoView = partnerRepServiceAdapter.isBankInfoExistsForPartnerReps(partnerRepIds, accessToken);

					LOGGER.debug(String.format("got %s bankinfos", bankInfoView.size()));

					return bankInfoView;

				} catch(final Exception e) {
					LOGGER.error("exception while retrieving bank information", e);
					throw new RuntimeException("exception while retrieving bank information", e);
				}
			}

		});


		CompletableFuture<Collection<MostRecentlyCommitedW9Info>>  mostRecenltyCommittedW9InfoFuture = CompletableFuture.supplyAsync(new Supplier<Collection<MostRecentlyCommitedW9Info>>() {

			@Override
			public Collection<MostRecentlyCommitedW9Info> get() {
				try {
					LOGGER.debug(String.format("calling w9infos on partnerRepServiceAdapter"));

					Collection<MostRecentlyCommitedW9Info> w9InfoView = partnerRepServiceAdapter.isW9InfoExistsForPartnerReps(partnerRepIds, accessToken);

					LOGGER.debug(String.format("got %s w9infos", w9InfoView.size()));

					return w9InfoView;
				} catch(final Exception e) {
					LOGGER.error("exception while retrieving w9 information", e);
					throw new RuntimeException("exception while retrieving w9 information", e);
				}
			}

		});

		CompletableFuture<Collection<PartnerRepContactInfo>>  partnerRepContactInfoFuture = CompletableFuture.supplyAsync(new Supplier<Collection<PartnerRepContactInfo>>() {

			@Override
			public Collection<PartnerRepContactInfo> get() {
				try {
					LOGGER.debug(String.format("calling contactInfos on partnerRepServiceAdapter"));

					Collection<PartnerRepContactInfo> contactInfoView = partnerRepServiceAdapter.isContactInfoExistsForPartnerReps(partnerRepIds, accessToken);

					LOGGER.debug(String.format(" got %s contactinfos", contactInfoView.size()));

					return contactInfoView;
				} catch(final Exception e) {
					LOGGER.error("exception while retrieving contact information", e);
					throw new RuntimeException("exception while retrieving contact information", e);
				}
			}

		});

		CompletableFuture.allOf(partnerRepsViewFuture,
				                mostRecenltyCommittedBankInfoFuture,
				                mostRecenltyCommittedW9InfoFuture,
				                partnerRepContactInfoFuture)
				         .join();



			List<PartnerRepSynopsisView> partnerRepsInfo = partnerRepsViewFuture
					.get()
					.stream()
					.sorted((view1, view2) -> view1.getId().getValue()
							.compareTo(view2.getId().getValue()))
					.collect(Collectors.toList());

			List<MostRecentlyCommitedBankInfo> mostRecentlyCommitedBankInfo = mostRecenltyCommittedBankInfoFuture
					.get()
					.stream()
					.sorted((bankInfo1, bankInfo2) -> bankInfo1
							.getPartnerRepUserId()
							.getValue()
							.compareTo(
									bankInfo2.getPartnerRepUserId().getValue()))
					.collect(Collectors.toList());

			List<MostRecentlyCommitedW9Info> mostRecenltyCommittedW9Info = mostRecenltyCommittedW9InfoFuture
					.get()
					.stream()
					.sorted((w9Info1, w9Info2) -> w9Info1
							.getPartnerRepUserId()
							.getValue()
							.compareTo(w9Info2.getPartnerRepUserId().getValue()))
					.collect(Collectors.toList());

			List<PartnerRepContactInfo> partnerRepContactInfo = partnerRepContactInfoFuture
					.get()
					.stream()
					.sorted((contactInfo1, contactInfo2) -> contactInfo1
							.getPartnerRepUserId()
							.getValue()
							.compareTo(
									contactInfo2.getPartnerRepUserId()
											.getValue()))
					.collect(Collectors.toList());


		for(int i=0; i < partnerRepsInfo.size(); i++) {

			PartnerRepSynopsisView partnerRepSynopsysView = partnerRepsInfo.get(i);
			MostRecentlyCommitedBankInfo bankInfo = mostRecentlyCommitedBankInfo.get(i);
			MostRecentlyCommitedW9Info w9Info = mostRecenltyCommittedW9Info.get(i);
			PartnerRepContactInfo contactInfo = partnerRepContactInfo.get(i);

			partnerRepInfos.add(new PartnerRepInfoViewImpl(partnerRepSynopsysView.getId(),
														   partnerRepSynopsysView.getFirstName().getValue(),
														   partnerRepSynopsysView.getLastName().getValue(),
														   bankInfo.isBankInfoExists(),
														   w9Info.isW9InfoExists(),
														   contactInfo.isContactInfoExists()));
		}


		} catch(final Exception e) {
			LOGGER.error("execute() encountered unexpected exception:", e);
			throw new RuntimeException("execute() encountered unexpected exception:", e);
		}

		return partnerRepInfos;

	}
}
