package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import com.precorconnect.partnersaleinvoiceservice.sdk.PartnerSaleInvoiceServiceSdkConfig;

public final class PartnerSaleInvoiceServiceAdapterConfigImpl implements PartnerSaleInvoiceServiceAdapterConfig {
	
	private final PartnerSaleInvoiceServiceSdkConfig partnerSaleInvoiceServiceSdkConfig;
	
	public PartnerSaleInvoiceServiceAdapterConfigImpl(final PartnerSaleInvoiceServiceSdkConfig partnerSaleInvoiceServiceSdkConfig) {
		this.partnerSaleInvoiceServiceSdkConfig = guardThat(
                "partnerSaleInvoiceServiceSdkConfig",
                partnerSaleInvoiceServiceSdkConfig
			  ).isNotNull()
			   .thenGetValue();
	}

	@Override
	public PartnerSaleInvoiceServiceSdkConfig getPartnerSaleInvoiceSdkConfig() {
		return partnerSaleInvoiceServiceSdkConfig;
	}


}
