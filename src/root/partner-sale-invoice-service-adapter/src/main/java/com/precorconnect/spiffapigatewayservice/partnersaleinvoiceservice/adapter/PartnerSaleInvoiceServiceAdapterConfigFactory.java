package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

public interface PartnerSaleInvoiceServiceAdapterConfigFactory {

	PartnerSaleInvoiceServiceAdapterConfig construct();
}
