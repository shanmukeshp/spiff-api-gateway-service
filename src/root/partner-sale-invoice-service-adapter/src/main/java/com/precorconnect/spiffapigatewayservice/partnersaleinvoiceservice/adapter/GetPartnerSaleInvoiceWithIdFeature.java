package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;

public interface GetPartnerSaleInvoiceWithIdFeature {
	PartnerSaleInvoiceView execute(
			@NonNull PartnerSaleInvoiceId partnerSaleInvoiceId,
			@NonNull OAuth2AccessToken oAuth2AccessToken)
			throws  AuthenticationException, AuthorizationException;

}
