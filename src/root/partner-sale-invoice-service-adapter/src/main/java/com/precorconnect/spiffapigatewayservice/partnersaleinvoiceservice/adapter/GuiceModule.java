package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.precorconnect.partnersaleinvoiceservice.sdk.PartnerSaleInvoiceServiceSdk;
import com.precorconnect.partnersaleinvoiceservice.sdk.PartnerSaleInvoiceServiceSdkImpl;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final PartnerSaleInvoiceServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull PartnerSaleInvoiceServiceAdapterConfig config
    ) {

        this.config =
                guardThat(
                        "config",
                        config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {


        bindFeatures();

        bindFactories();
    }

    private void bindFactories() {
    	 bind(AddPartnerSaleInvoiceReqFactory.class)
         .to(AddPartnerSaleInvoiceReqFactoryImpl.class);

    	 bind(PartnerSaleInvoiceViewFactory.class)
         	.to(PartnerSaleInvoiceViewFactoryImpl.class);

	}

	private void bindFeatures() {
    	 bind(AddPartnerSaleInvoiceFeature.class)
         .to(AddPartnerSaleInvoiceFeatureImpl.class);

    	 bind(GetPartnerSaleInvoiceWithIdFeature.class)
         	.to(GetPartnerSaleInvoiceWithIdFeatureImpl.class);
    }


    @Provides
    @Singleton
    public PartnerSaleInvoiceServiceSdk partnerSaleInvoiceServiceSdk() {

        return
                new PartnerSaleInvoiceServiceSdkImpl(
                        config.getPartnerSaleInvoiceSdkConfig()
                );

    }

    @Provides
    @Singleton
    public PartnerSaleInvoiceServiceAdapterConfig partnerSaleInvoiceServiceAdapterConfig() {

        return config;

    }



}
