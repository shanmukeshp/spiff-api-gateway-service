package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public interface AddPartnerSaleInvoiceFeature {

	PartnerSaleInvoiceId execute(
			@NonNull UploadPartnerSaleInvoiceReq request,
			@NonNull OAuth2AccessToken oAuth2AccessToken)
			throws  AuthenticationException, AuthorizationException;
}
