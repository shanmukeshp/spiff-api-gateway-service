package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.spiffapigatewayservice.core.PartnerSaleInvoiceServiceAdapter;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public final class PartnerSaleInvoiceServiceAdapterImpl implements PartnerSaleInvoiceServiceAdapter {

	 /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public PartnerSaleInvoiceServiceAdapterImpl(
            @NonNull PartnerSaleInvoiceServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }

	@Override
	public PartnerSaleInvoiceId addPartnerSaleInvoice(
			@NonNull UploadPartnerSaleInvoiceReq request,
			@NonNull OAuth2AccessToken oAuth2AccessToken)
			throws AuthenticationException, AuthorizationException {
        return injector
        .getInstance(AddPartnerSaleInvoiceFeature.class)
        .execute(
        		request,
        		oAuth2AccessToken
        	);
	}

	@Override
	public PartnerSaleInvoiceView getPartnerSaleInvoiceWithId(
			@NonNull PartnerSaleInvoiceId partnerSaleInvoiceId,
			@NonNull OAuth2AccessToken oAuth2AccessToken)
					throws AuthenticationException, AuthorizationException {
        return injector
        .getInstance(GetPartnerSaleInvoiceWithIdFeature.class)
        .execute(partnerSaleInvoiceId, oAuth2AccessToken);
	}

}
