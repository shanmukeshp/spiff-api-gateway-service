package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.precorconnect.FileContentImpl;
import com.precorconnect.FileExtensionImpl;
import com.precorconnect.partnersaleinvoiceservice.AddPartnerSaleInvoiceReq;
import com.precorconnect.partnersaleinvoiceservice.AddPartnerSaleInvoiceReqImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public final class AddPartnerSaleInvoiceReqFactoryImpl implements AddPartnerSaleInvoiceReqFactory {

	@Override
	public AddPartnerSaleInvoiceReq construct(
			UploadPartnerSaleInvoiceReq uploadPartnerSaleInvoiceReq) {

		String fileExtension = uploadPartnerSaleInvoiceReq
									.getFile()
									.getName()
									.substring(
											uploadPartnerSaleInvoiceReq
												.getFile()
												.getName()
												.lastIndexOf( ".") + 1);

		AddPartnerSaleInvoiceReq addPartnerSaleInvoiceReq;

		FileInputStream fileInputStream=null;
		File file = uploadPartnerSaleInvoiceReq.getFile();

        byte[] bFile = new byte[(int) file.length()];

		try {
			fileInputStream = new FileInputStream(file);
		    fileInputStream.read(bFile);
		    fileInputStream.close();

		    for (int i = 0; i < bFile.length; i++) {
		       	System.out.print((char)bFile[i]);
	            }

			addPartnerSaleInvoiceReq = new AddPartnerSaleInvoiceReqImpl(
												uploadPartnerSaleInvoiceReq.getNumber(),
												new FileContentImpl(bFile),
												new FileExtensionImpl(fileExtension),
												uploadPartnerSaleInvoiceReq.getPartnerSaleRegId()
												);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException("exception occured in multipartfile parsing",e);
		} catch (IOException e) {
			throw new RuntimeException("exception occured in multipartfile parsing",e);
		}
		return
				addPartnerSaleInvoiceReq;
	}

}
