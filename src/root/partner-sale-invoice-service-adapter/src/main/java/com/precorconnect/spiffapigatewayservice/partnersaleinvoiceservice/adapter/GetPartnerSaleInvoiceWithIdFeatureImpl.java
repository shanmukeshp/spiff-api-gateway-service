package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.partnersaleinvoiceservice.sdk.PartnerSaleInvoiceServiceSdk;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;

public final class GetPartnerSaleInvoiceWithIdFeatureImpl implements GetPartnerSaleInvoiceWithIdFeature {

	private final PartnerSaleInvoiceServiceSdk partnerSaleInvoiceServiceSdk;

	private final PartnerSaleInvoiceViewFactory partnerSaleInvoiceViewFactory;

	@Inject
	public GetPartnerSaleInvoiceWithIdFeatureImpl(final PartnerSaleInvoiceServiceSdk partnerSaleInvoiceServiceSdk,
												  final PartnerSaleInvoiceViewFactory partnerSaleInvoiceViewFactory) {
		this.partnerSaleInvoiceServiceSdk = guardThat(
                "partnerSaleInvoiceServiceSdk",
                partnerSaleInvoiceServiceSdk
			  ).isNotNull()
			   .thenGetValue();

		this.partnerSaleInvoiceViewFactory = guardThat(
                "partnerSaleInvoiceViewFactory",
                partnerSaleInvoiceViewFactory
			  ).isNotNull()
			   .thenGetValue();

	}

	@Override
	public PartnerSaleInvoiceView execute(
			@NonNull PartnerSaleInvoiceId partnerSaleInvoiceId,
			@NonNull OAuth2AccessToken oAuth2AccessToken)
			throws AuthenticationException, AuthorizationException {

		com.precorconnect.partnersaleinvoiceservice.webapi.PartnerSaleInvoiceView partnerSaleInvoiceview = partnerSaleInvoiceServiceSdk
				.getPartnerSaleInvoiceWithId(partnerSaleInvoiceId,
						oAuth2AccessToken);

		return partnerSaleInvoiceViewFactory.construct(partnerSaleInvoiceview);
	}


}
