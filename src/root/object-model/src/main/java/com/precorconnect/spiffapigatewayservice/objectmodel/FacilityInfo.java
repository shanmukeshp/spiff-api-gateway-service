package com.precorconnect.spiffapigatewayservice.objectmodel;

public interface FacilityInfo {

	FacilityId getFacilityId();
	
	FacilityName getFacilityName();
}
