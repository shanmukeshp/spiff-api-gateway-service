package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.UserId;

public class ClaimSpiffViewImpl implements ClaimSpiffView {


	private final ClaimSpiffId claimId;

    private final PartnerSaleRegId partnerSaleRegistrationId;

    private final AccountId partnerAccountId;

    private final UserId partnerRepUserId;

    private final FirstName partnerRepFirstName;

    private final LastName partnerRepLastName;

    private final InstallDate installDate;

    private final SpiffAmount spiffAmount;

    private final SpiffClaimedDate spiffClaimedDate;

    private final FacilityName facilityName;

    private final InvoiceNumber invoiceNumber;

    public ClaimSpiffViewImpl(
			@NonNull ClaimSpiffId claimId,
			@NonNull PartnerSaleRegId partnerSaleRegistrationId,
			@NonNull AccountId partnerAccountId,
			@NonNull UserId partnerRepUserId,
			@NonNull InstallDate installDate,
			@NonNull SpiffAmount spiffAmount,
			@NonNull SpiffClaimedDate spiffClaimedDate,
			@NonNull FacilityName facilityName,
			@NonNull InvoiceNumber invoiceNumber,
			@NonNull FirstName partnerRepFirstName,
			@NonNull LastName partnerRepLastName
			) {

		this.claimId =
                guardThat(
                        "claimId",
                         claimId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                         partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                         partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                         partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

		this.installDate = installDate;


		this.spiffAmount =
                guardThat(
                        "spiffAmount",
                         spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffClaimedDate =
                guardThat(
                        "spiffClaimedDate",
                         spiffClaimedDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.facilityName =
                guardThat(
                        "facilityName",
                         facilityName
                )
                        .isNotNull()
                        .thenGetValue();


		this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                         invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerRepFirstName =
                guardThat(
                        "partnerRepFirstName",
                        partnerRepFirstName
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerRepLastName =
                guardThat(
                        "partnerRepLastName",
                        partnerRepLastName
                )
                        .isNotNull()
                        .thenGetValue();

	}

	@Override
	public ClaimSpiffId getClaimId() {
		return claimId;
	}

	@Override
	public PartnerSaleRegId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	@Override
	public AccountId getPartnerAccountId() {
		return partnerAccountId;
	}

	@Override
	public UserId getPartnerRepUserId() {
		return partnerRepUserId;
	}

	@Override
	public InstallDate getInstallDate() {
		return installDate;
	}

	@Override
	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	@Override
	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	@Override
	public FacilityName getFacilityName() {
		return facilityName;
	}

	@Override
	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

	@Override
	public FirstName getPartnerRepFirstName() {
		return partnerRepFirstName;
	}

	@Override
	public LastName getPartnerRepLastName() {
		return partnerRepLastName;
	}

}