package com.precorconnect.spiffapigatewayservice.objectmodel;

public interface FacilityName {

	String getValue();
}
