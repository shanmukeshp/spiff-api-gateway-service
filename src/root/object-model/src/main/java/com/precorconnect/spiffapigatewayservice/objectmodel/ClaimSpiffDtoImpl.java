package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AccountId;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.UserId;


public class ClaimSpiffDtoImpl implements ClaimSpiffDto {

    /*
    fields
    */
    private PartnerSaleRegId partnerSaleRegistrationId;

    private final AccountId partnerAccountId;

    private final UserId partnerRepUserId;

	private final FirstName firstName;

	private final LastName lastName;

    private InstallDate installDate;

    private SpiffAmount spiffAmount;

    private SpiffClaimedDate spiffClaimedDate;

    private FacilityName facilityName;

    private InvoiceNumber invoiceNumber;

    /*
    constructors
    */
    public ClaimSpiffDtoImpl(
            @NonNull final PartnerSaleRegId partnerSaleRegistrationId,
            @NonNull final AccountId partnerAccountId,
            @NonNull final UserId partnerRepUserId,
            @NonNull final FirstName firstName,
			@NonNull final LastName lastName,
            @NonNull final InstallDate installDate,
            @NonNull final SpiffAmount spiffAmount,
            @Nullable final SpiffClaimedDate spiffClaimedDate,
            @NonNull final FacilityName facilityName,
            @NonNull final InvoiceNumber invoiceNumber
    ) {

    	this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                         partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                        partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                         partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.firstName =
	               guardThat(
	                       "firstName",
	                       firstName
	               )
	                       .isNotNull()
	                       .thenGetValue();

		this.lastName =
	               guardThat(
	                       "lastName",
	                       lastName
	               )
	                       .isNotNull()
	                       .thenGetValue();

    	this.installDate =
                guardThat(
                        "installDate",
                         installDate
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffAmount =
                guardThat(
                        "spiffAmount",
                         spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffClaimedDate = spiffClaimedDate;

    	this.facilityName=
                guardThat(
                        "facilityName",
                         facilityName
                )
                        .isNotNull()
                        .thenGetValue();

    	this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                         invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter & setter methods
    */
    @Override
	public PartnerSaleRegId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public void setPartnerSaleRegistrationId(
			PartnerSaleRegId partnerSaleRegistrationId) {
		this.partnerSaleRegistrationId = partnerSaleRegistrationId;
	}

	@Override
	public InstallDate getInstallDate() {
		return installDate;
	}

	public void setInstallDate(InstallDate installDate) {
		this.installDate = installDate;
	}

	@Override
	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	public void setSpiffAmount(SpiffAmount spiffAmount) {
		this.spiffAmount = spiffAmount;
	}

	@Override
	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public void setSpiffClaimedDate(SpiffClaimedDate spiffClaimedDate) {
		this.spiffClaimedDate = spiffClaimedDate;
	}


	@Override
	public FacilityName getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(FacilityName facilityName) {
		this.facilityName = facilityName;
	}

	@Override
	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(InvoiceNumber invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	@Override
	public AccountId getPartnerAccountId() {
		return partnerAccountId;
	}

	@Override
	public UserId getPartnerRepUserId() {
		return partnerRepUserId;
	}

	@Override
	public FirstName getFirstName() {
		return firstName;
	}

	@Override
	public LastName getLastName() {
		return lastName;
	}

}
