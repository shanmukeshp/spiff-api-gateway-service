package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public final class InvoiceUrlImpl implements InvoiceUrl {
	
	private String invoiceUrl;
	
	public InvoiceUrlImpl(@NonNull final String invoiceUrl) {
		this.invoiceUrl =  guardThat(
								"invoiceUrl",
								invoiceUrl
							).isNotNull()
							 .thenGetValue();
	
	}

	@Override
	public String getValue() {
		return invoiceUrl;
	}

}
