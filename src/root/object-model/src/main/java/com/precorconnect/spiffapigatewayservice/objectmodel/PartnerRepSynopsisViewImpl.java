package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.UserId;
import org.checkerframework.checker.nullness.qual.NonNull;

public class PartnerRepSynopsisViewImpl
        implements PartnerRepSynopsisView {

    /*
    fields
     */
    private final UserId id;

    private final FirstName firstName;

    private final LastName lastName;

    /*
    constructors
     */
    public PartnerRepSynopsisViewImpl(
            @NonNull UserId id,
            @NonNull FirstName firstName,
            @NonNull LastName lastName
    ) {

        if (null == id) {
            throw new IllegalArgumentException("id cannot be null");
        }
        this.id = id;

        if (null == firstName) {
            throw new IllegalArgumentException("firstName cannot be null");
        }
        this.firstName = firstName;

        if (null == lastName) {
            throw new IllegalArgumentException("lastName cannot be null");
        }
        this.lastName = lastName;

    }

    /*
    getter methods
    */
    @Override
    public UserId getId() {
        return id;
    }

    @Override
    public FirstName getFirstName() {
        return firstName;
    }

    @Override
    public LastName getLastName() {
        return lastName;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PartnerRepSynopsisViewImpl that = (PartnerRepSynopsisViewImpl) o;

        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + id.hashCode();
        return result;
    }
}
