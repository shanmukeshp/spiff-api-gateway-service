package com.precorconnect.spiffapigatewayservice.objectmodel;

import java.util.Date;

public interface InstallDate {

	Date getValue();
}
