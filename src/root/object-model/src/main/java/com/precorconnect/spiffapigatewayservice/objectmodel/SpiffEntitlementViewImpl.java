package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.UserId;

public final class SpiffEntitlementViewImpl implements SpiffEntitlementView {

	private SpiffEntitlementId spiffEntitlementId;

	private PartnerSaleRegId partnerSaleRegistrationId;

	private InstallDate installDate;

	private SpiffAmount spiffAmount;

	private UserId partnerRepUserId;

	private FirstName firstName;

	private LastName lastName;

	private InvoiceInfo invoiceInfo;

	private FacilityName facilityName;

	public SpiffEntitlementViewImpl(@NonNull final PartnerSaleRegId partnerSaleRegistrationId,
							  @NonNull final InstallDate installDate,
							  @NonNull final SpiffAmount spiffAmount,
							  @NonNull final UserId partnerRepUserId,
							  @NonNull final FirstName firstName,
							  @NonNull final LastName lastName,
							  @NonNull final InvoiceInfo invoiceInfo,
							  @NonNull final FacilityName facilityName,
							  @NonNull final SpiffEntitlementId spiffEntitlementId) {

		this.partnerSaleRegistrationId =  guardThat(
        										"partnerSaleRegistrationId",
        										partnerSaleRegistrationId
												).isNotNull()
												 .thenGetValue();

		this.installDate =  guardThat(
										"installDate",
										installDate
									).isNotNull()
									 .thenGetValue();

		this.spiffAmount =  guardThat(
									"spiffAmount",
									spiffAmount
								).isNotNull()
								 .thenGetValue();

		this.partnerRepUserId =  guardThat(
									"partnerRepUserId",
									partnerRepUserId
								).isNotNull()
								 .thenGetValue();


		this.firstName =
			               guardThat(
			                       "firstName",
			                       firstName
			               )
			                       .isNotNull()
			                       .thenGetValue();

		this.lastName =
			               guardThat(
			                       "lastName",
			                       lastName
			               )
			                       .isNotNull()
			                       .thenGetValue();


		this.invoiceInfo =  guardThat(
									"invoiceInfo",
									invoiceInfo
							    ).isNotNull()
							     .thenGetValue();


		this.facilityName = guardThat(
									"facilityName",
									facilityName
								).isNotNull()
								.thenGetValue();

		this.spiffEntitlementId = guardThat(
									"spiffEntitlementId",
									spiffEntitlementId
								).isNotNull()
								 .thenGetValue();

	}

	@Override
	public PartnerSaleRegId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	@Override
	public InstallDate getInstallDate() {
		return installDate;
	}

	@Override
	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	@Override
	public UserId getPartnerRepuserId() {
		return partnerRepUserId;
	}

	@Override
	public FirstName getFirstName() {
		return firstName;
	}

	@Override
	public LastName getLastName() {
		return lastName;
	}

	@Override
	public InvoiceInfo getInvoiceInfo() {
		return invoiceInfo;
	}

	@Override
	public FacilityName getFacilityName() {
		return facilityName;
	}

	@Override
	public SpiffEntitlementId getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

}
