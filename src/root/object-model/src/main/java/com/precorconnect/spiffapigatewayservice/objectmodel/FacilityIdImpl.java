package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public final class FacilityIdImpl implements FacilityId {

	private Long facilityId;
	
	public FacilityIdImpl(@NonNull final Long facilityId) {
		this.facilityId =  guardThat(
        					"facilityId",
        					facilityId
							).isNotNull()
							 .thenGetValue();
	
	}

	@Override
	public Long getValue() {
		return facilityId;
	}
	
	
}
