package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.io.File;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.PartnerSaleInvoiceNumber;
import com.precorconnect.PartnerSaleRegId;

public class UploadPartnerSaleInvoiceReqImpl implements UploadPartnerSaleInvoiceReq {


	private final PartnerSaleInvoiceNumber number;

	private final PartnerSaleRegId partnerSaleRegId;

	private final File file;

	public UploadPartnerSaleInvoiceReqImpl(
			@NonNull final PartnerSaleInvoiceNumber number,
			@NonNull final PartnerSaleRegId partnerSaleRegId,
			@NonNull final File file
			) {

		this.number =
                guardThat(
                        "number",
                        number
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegId =  guardThat(
                							"partnerSaleRegId",
                							partnerSaleRegId
										  )
										   .isNotNull()
										   .thenGetValue();

    	this.file =
                guardThat(
                        "file",
                        file
                )
                        .isNotNull()
                        .thenGetValue();

	}

	@Override
	public PartnerSaleInvoiceNumber getNumber() {
		return number;
	}

	@Override
	public PartnerSaleRegId getPartnerSaleRegId() {
		return partnerSaleRegId;
	}

	@Override
	public File getFile() {
		return file;
	}

}
