package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.UserId;

public class ClaimSpiffSynopysisViewImpl implements ClaimSpiffSynopysisView {


	private final ClaimSpiffId claimId;

    private final PartnerSaleRegId partnerSaleRegistrationId;

    private final AccountId partnerAccountId;

    private final UserId partnerRepUserId;

	private FirstName firstName;

	private LastName lastName;

    private final InstallDate installDate;

    private final SpiffAmount spiffAmount;

    private final SpiffClaimedDate spiffClaimedDate;

    private final FacilityName facilityName;

    private final InvoiceNumber invoiceNumber;

    public ClaimSpiffSynopysisViewImpl(
			@NonNull ClaimSpiffId claimId,
			@NonNull PartnerSaleRegId partnerSaleRegistrationId,
			@NonNull AccountId partnerAccountId,
			@NonNull UserId partnerRepUserId,
			@NonNull final FirstName firstName,
			@NonNull final LastName lastName,
			@NonNull InstallDate installDate,
			@NonNull SpiffAmount spiffAmount,
			@NonNull SpiffClaimedDate spiffClaimedDate,
			@NonNull FacilityName facilityName,
			@NonNull InvoiceNumber invoiceNumber
			) {

		this.claimId =
                guardThat(
                        "claimId",
                         claimId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                         partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                         partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                         partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

		this.firstName =
	               guardThat(
	                       "firstName",
	                       firstName
	               )
	                       .isNotNull()
	                       .thenGetValue();

		this.lastName =
	               guardThat(
	                       "lastName",
	                       lastName
	               )
	                       .isNotNull()
	                       .thenGetValue();

		this.installDate = installDate;


		this.spiffAmount =
                guardThat(
                        "spiffAmount",
                         spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffClaimedDate =
                guardThat(
                        "spiffClaimedDate",
                         spiffClaimedDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.facilityName =
                guardThat(
                        "facilityName",
                         facilityName
                )
                        .isNotNull()
                        .thenGetValue();


		this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                         invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

	}

	@Override
	public ClaimSpiffId getClaimId() {
		return claimId;
	}

	@Override
	public PartnerSaleRegId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	@Override
	public AccountId getPartnerAccountId() {
		return partnerAccountId;
	}

	@Override
	public UserId getPartnerRepUserId() {
		return partnerRepUserId;
	}

	@Override
	public FirstName getFirstName() {
		return firstName;
	}

	@Override
	public LastName getLastName() {
		return lastName;
	}

	@Override
	public InstallDate getInstallDate() {
		return installDate;
	}

	@Override
	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	@Override
	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	@Override
	public FacilityName getFacilityName() {
		return facilityName;
	}

	@Override
	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}


}