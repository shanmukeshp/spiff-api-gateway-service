package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public final class SpiffAmountImpl implements SpiffAmount {
	
	private Double amount;
	
	public SpiffAmountImpl(@NonNull final Double amount) {
		this.amount =  guardThat(
        					"amount",
        					amount
							).isNotNull()
							 .thenGetValue();
	}

	@Override
	public Double getValue() {
		return amount;
	}

}
