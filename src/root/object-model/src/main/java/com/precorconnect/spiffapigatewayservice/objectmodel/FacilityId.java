package com.precorconnect.spiffapigatewayservice.objectmodel;

public interface FacilityId {

	Long getValue();
}
