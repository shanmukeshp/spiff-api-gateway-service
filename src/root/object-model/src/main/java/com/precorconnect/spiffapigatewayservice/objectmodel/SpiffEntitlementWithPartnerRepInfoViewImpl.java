package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.PartnerSaleRegId;

public final class SpiffEntitlementWithPartnerRepInfoViewImpl implements SpiffEntitlementWithPartnerRepInfoView {

	private SpiffEntitlementId spiffEntitlementId;

	private PartnerSaleRegId partnerSaleRegistrationId;

	private InstallDate installDate;

	private SpiffAmount spiffAmount;

	private PartnerRepInfoView partnerRepInfoView;

	private InvoiceInfo invoiceInfo;

	private FacilityName facilityName;

	public SpiffEntitlementWithPartnerRepInfoViewImpl(
			@NonNull final PartnerSaleRegId partnerSaleRegistrationId,
			@NonNull final InstallDate installDate,
			@NonNull final SpiffAmount spiffAmount,
			@NonNull final InvoiceInfo invoiceInfo,
			@NonNull final FacilityName facilityName,
			@NonNull final SpiffEntitlementId spiffEntitlementId,
			@NonNull final PartnerRepInfoView partnerRepInfoView
	) {

		this.partnerSaleRegistrationId =  guardThat(
        										"partnerSaleRegistrationId",
        										partnerSaleRegistrationId
												).isNotNull()
												 .thenGetValue();

		this.installDate =  guardThat(
										"installDate",
										installDate
									).isNotNull()
									 .thenGetValue();

		this.spiffAmount =  guardThat(
									"spiffAmount",
									spiffAmount
								).isNotNull()
								 .thenGetValue();

		this.partnerRepInfoView =  guardThat(
									"partnerRepInfoView",
									partnerRepInfoView
								).isNotNull()
								 .thenGetValue();


		this.invoiceInfo =  guardThat(
									"invoiceInfo",
									invoiceInfo
							    ).isNotNull()
							     .thenGetValue();


		this.facilityName = guardThat(
									"facilityName",
									facilityName
								).isNotNull()
								.thenGetValue();

		this.spiffEntitlementId = guardThat(
									"spiffEntitlementId",
									spiffEntitlementId
								).isNotNull()
								.thenGetValue();

	}

	@Override
	public SpiffEntitlementId getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	@Override
	public PartnerSaleRegId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	@Override
	public InstallDate getInstallDate() {
		return installDate;
	}

	@Override
	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	@Override
	public PartnerRepInfoView getPartnerRepInfoView() {
		return partnerRepInfoView;
	}

	@Override
	public InvoiceInfo getInvoiceInfo() {
		return invoiceInfo;
	}

	@Override
	public FacilityName getFacilityName() {
		return facilityName;
	}


}
