package com.precorconnect.spiffapigatewayservice.objectmodel;

public interface SpiffEntitlementId {

	public Long getValue();

}
