package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class PartnerSaleInvoiceIdImpl implements PartnerSaleInvoiceId {
    private final Integer value;
    
    public PartnerSaleInvoiceIdImpl(
    		@NonNull Integer value
    		){
    	
    	this.value =
                guardThat(
                        "partnerSaleInvoiceId",
                        value
                )
                        .isNotNull()
                        .thenGetValue();

    }
    
	@Override
	public Integer getValue() {

		return value;
	}

}
