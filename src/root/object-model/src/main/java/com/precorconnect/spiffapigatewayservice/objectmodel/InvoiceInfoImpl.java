package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public final class InvoiceInfoImpl implements InvoiceInfo {
	
	private InvoiceUrl invoiceUrl;
	
	private InvoiceNumber invoiceNumber;
	
	public InvoiceInfoImpl(@NonNull final InvoiceUrl invoiceUrl, @NonNull final InvoiceNumber invoiceNumber) {
		this.invoiceUrl =  guardThat(
									"invoiceUrl",
									invoiceUrl
									).isNotNull()
									.thenGetValue();

		this.invoiceNumber =  guardThat(
									"invoiceNumber",
									invoiceNumber
								   ).isNotNull()
								   	.thenGetValue();

	}

	@Override
	public InvoiceUrl getInvoiceUrl() {
		return invoiceUrl;
	}

	@Override
	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

}
