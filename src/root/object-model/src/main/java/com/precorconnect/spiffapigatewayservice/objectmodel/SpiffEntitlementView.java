package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.UserId;


public interface SpiffEntitlementView {

	SpiffEntitlementId getSpiffEntitlementId();

	PartnerSaleRegId getPartnerSaleRegistrationId();

	InstallDate getInstallDate();

	SpiffAmount getSpiffAmount();

	UserId getPartnerRepuserId();

	FirstName getFirstName();

	LastName getLastName();

	InvoiceInfo getInvoiceInfo();

	FacilityName getFacilityName();

}
