package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.UserId;

public interface PartnerRepSynopsisView {

    UserId getId();

    FirstName getFirstName();

    LastName getLastName();

}
