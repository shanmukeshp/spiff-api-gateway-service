package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URL;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.PartnerSaleInvoiceNumber;
import com.precorconnect.PartnerSaleRegId;

public class PartnerSaleInvoiceViewImpl implements PartnerSaleInvoiceView {


	private final PartnerSaleInvoiceId id;

	private final PartnerSaleInvoiceNumber number;

	private final URL fileUrl;

	private final PartnerSaleRegId partnerSaleRegistrationId;

	
	public PartnerSaleInvoiceViewImpl(@NonNull final PartnerSaleInvoiceId id,
			@NonNull final PartnerSaleInvoiceNumber number,
			@NonNull final URL fileUrl,
			@Nullable final PartnerSaleRegId partnerSaleRegistrationId) {

		this.id =
                guardThat(
                        "id",
                        id
                )
                        .isNotNull()
                        .thenGetValue();

    	this.number =
                guardThat(
                        "number",
                        number
                )
                        .isNotNull()
                        .thenGetValue();

    	this.fileUrl =
                guardThat(
                        "fileUrl",
                        fileUrl
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegistrationId = partnerSaleRegistrationId;

	}

	/*
	 * getter methods
	 */
	@Override
	public PartnerSaleInvoiceId getId() {
		return id;
	}

	@Override
	public PartnerSaleInvoiceNumber getNumber() {
		return number;
	}

	@Override
	public URL getFileUrl() {
		return fileUrl;
	}

	@Override
	public PartnerSaleRegId getPartnerSaleRegId() {
		return partnerSaleRegistrationId;
	}

	/*
	 * equality methods
	 */
	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		PartnerSaleInvoiceViewImpl that = (PartnerSaleInvoiceViewImpl) o;

		if (!id.equals(that.id)) {
			return false;
		}
		if (!number.equals(that.number)) {
			return false;
		}
		if (!fileUrl.equals(that.fileUrl)) {
			return false;
		}
		return partnerSaleRegistrationId.equals(that.partnerSaleRegistrationId);

	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + number.hashCode();
		result = 31 * result + fileUrl.hashCode();
		result = 31 * result + partnerSaleRegistrationId.hashCode();
		return result;
	}
}
