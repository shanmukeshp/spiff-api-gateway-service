package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.AccountId;
import com.precorconnect.PartnerRepAssociationId;
import com.precorconnect.UserId;

public interface PartnerRepAssociationSynopsisView {

    PartnerRepAssociationId getId();

    AccountId getPartnerId();

    UserId getRepId();

}
