package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Date;

public final class SpiffClaimedDateImpl implements SpiffClaimedDate {

	private Date value;
	
	public SpiffClaimedDateImpl(final Date value) {
	    this.value =
	            guardThat(
	                    "claimSpiffDate",
	                    value
	            )
	                    .isNotNull()
	                    .thenGetValue();

	}
	
	@Override
	public Date getValue() {
		return value;
	}

}
