package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public final class InvoiceNumberImpl implements InvoiceNumber {
	
	private String invoiceNumber;
	
	public InvoiceNumberImpl(@NonNull final String invoiceNumber) {
		this.invoiceNumber =  guardThat(
								"invoiceNumber",
								invoiceNumber
								).isNotNull()
								 .thenGetValue();
	
	}

	@Override
	public String getValue() {
		return invoiceNumber;
	}

}
