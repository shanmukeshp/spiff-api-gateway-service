package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import com.precorconnect.UserId;

public final class PartnerRepContactInfoImpl implements PartnerRepContactInfo {
	
	private final UserId partnerRepUserId;
	
	private final boolean isBankInfoExists;
	
	public PartnerRepContactInfoImpl(final UserId partnerRepUserId, final boolean isBankInfoExists) {
        this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                        partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

        this.isBankInfoExists =
                guardThat(
                        "isBankInfoExists",
                        isBankInfoExists
                )
                        .isNotNull()
                        .thenGetValue();
	
	}

	@Override
	public UserId getPartnerRepUserId() {
		return partnerRepUserId;
	}

	@Override
	public boolean isContactInfoExists() {
		return isBankInfoExists;
	}

}
