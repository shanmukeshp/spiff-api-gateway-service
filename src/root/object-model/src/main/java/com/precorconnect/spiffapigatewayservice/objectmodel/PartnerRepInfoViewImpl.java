package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.UserId;

public final class PartnerRepInfoViewImpl implements PartnerRepInfoView {
	
	private UserId userId;
	
	private String firstName;
	
	private String lastName;
	
	private boolean isBankInfoExists;
	
	private boolean isW9InfoExists;
	
	private boolean isContactInfoExists;
	
	public PartnerRepInfoViewImpl(@NonNull final UserId userId, 
								  @NonNull final  String firstName, 
								  @NonNull final String lastName, 
								  @NonNull final boolean isBankInfoExists,
								  @NonNull final boolean isW9InfoExists,
								  @NonNull final boolean isContactInfoExists) {
		this.userId =  guardThat(
                		"userId",
                		 userId
						).isNotNull()
						 .thenGetValue();

		this.firstName =  guardThat(
        				 "firstName",
        				 firstName
						).isNotNull()
						.thenGetValue();

		this.lastName =  guardThat(
        				"lastName",
        				lastName
						).isNotNull()
						 .thenGetValue();
		
		this.isBankInfoExists = isBankInfoExists;
		
		this.isW9InfoExists = isW9InfoExists;
		
		this.isContactInfoExists = isContactInfoExists;

	}

	@Override
	public UserId getUserId() {
		return userId;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public boolean isBankInfoExists() {
		return isBankInfoExists;
	}

	@Override
	public boolean isW9InfoExists() {
		return isW9InfoExists;
	}

	@Override
	public boolean isContactInfoExists() {
		return isContactInfoExists;
	}
	
	

}
