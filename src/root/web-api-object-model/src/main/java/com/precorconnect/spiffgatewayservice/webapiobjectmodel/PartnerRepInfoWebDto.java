package com.precorconnect.spiffgatewayservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class PartnerRepInfoWebDto {
	/*
	 fields
	 */
	private final String userId;

	private final String firstName;

	private final String lastName;

	private final boolean isBankInfoExists;

	private final boolean isW9InfoExists;

	private final boolean isContactInfoExists;

	/*
	 Constructors
	 */
	public PartnerRepInfoWebDto(){

		userId = null;

		firstName = null;

		lastName = null;

		isBankInfoExists = false;

		isW9InfoExists = false;

		isContactInfoExists = false;
	}

	public PartnerRepInfoWebDto(
			@NonNull final String userId,
			@NonNull final String firstName,
			@NonNull final String lastName,
			@NonNull final boolean isBankInfoExists,
			@NonNull final boolean isW9InfoExists,
			@NonNull final boolean isContactInfoExists
			){

		this.userId =
               guardThat(
                       "userId",
                       userId
               )
                       .isNotNull()
                       .thenGetValue();

		this.firstName =
               guardThat(
                       "firstName",
                       firstName
               )
                       .isNotNull()
                       .thenGetValue();

		this.lastName =
               guardThat(
                       "lastName",
                       lastName
               )
                       .isNotNull()
                       .thenGetValue();

		this.isBankInfoExists =
               guardThat(
                       "isBankInfoExists",
                       isBankInfoExists
               )
                       .isNotNull()
                       .thenGetValue();

		this.isW9InfoExists =
               guardThat(
                       "isW9InfoExists",
                       isW9InfoExists
               )
                       .isNotNull()
                       .thenGetValue();

		this.isContactInfoExists =
               guardThat(
                       "isContactInfoExists",
                       isContactInfoExists
               )
                       .isNotNull()
                       .thenGetValue();
	}

	/*
	 getter methods
	*/

	public String getUserId() {
		return userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public boolean isBankInfoExists() {
		return isBankInfoExists;
	}

	public boolean isW9InfoExists() {
		return isW9InfoExists;
	}

	public boolean isContactInfoExists() {
		return isContactInfoExists;
	}

}
