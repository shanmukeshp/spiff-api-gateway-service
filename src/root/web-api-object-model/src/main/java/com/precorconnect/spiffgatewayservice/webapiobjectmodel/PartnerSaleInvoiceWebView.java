package com.precorconnect.spiffgatewayservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class PartnerSaleInvoiceWebView {

	/*
	 fields
	 */

	private final String partnerSaleInvoiceId;

	private final String partnerSaleInvoiceNumber;

	private final String fileUrl;

	private final Long partnerSaleRegId;

	/*
	 Constructors
	 */
	public PartnerSaleInvoiceWebView(){

		partnerSaleInvoiceId = null;

		partnerSaleInvoiceNumber = null;

		fileUrl = null;

		partnerSaleRegId = 0L;

	}

	public PartnerSaleInvoiceWebView(
			@NonNull final String partnerSaleInvoiceId,
			@NonNull final String partnerSaleInvoiceNumber,
			@NonNull final String fileUrl,
			@NonNull final Long partnerSaleRegId
			){

		this.partnerSaleInvoiceId =
                guardThat(
                        "partnerSaleInvoiceId",
                        partnerSaleInvoiceId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleInvoiceNumber =
                guardThat(
                        "partnerSaleInvoiceNumber",
                        partnerSaleInvoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

		this.fileUrl =
                guardThat(
                        "fileUrl",
                        fileUrl
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegId =
                guardThat(
                        "partnerSaleRegId",
                        partnerSaleRegId
                )
                        .isNotNull()
                        .thenGetValue();

	}

	public String getPartnerSaleInvoiceId() {
		return partnerSaleInvoiceId;
	}

	public String getPartnerSaleInvoiceNumber() {
		return partnerSaleInvoiceNumber;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public Long getPartnerSaleRegId() {
		return partnerSaleRegId;
	}


}
