package com.precorconnect.spiffgatewayservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class SpiffEntitlementWithPartnerRepInfoWebView {

	/*
	 fields
	 */

	private final Long spiffEntitlementId;

	private final Long partnerSaleRegistrationId;

	private final String installDate;

	private final Double spiffAmount;

	private final String partnerRepUserId;

	private final String firstName;

	private final String lastName;

	private final boolean isBankInfoExists;

	private final boolean isW9InfoExists;

	private final boolean isContactInfoExists;

	private final String invoiceUrl;

	private final String invoiceNumber;

	private final String facilityName;

	/*
	 Constructors
	 */
	public SpiffEntitlementWithPartnerRepInfoWebView(){

		spiffEntitlementId = 0L;

		partnerSaleRegistrationId = 0L;

		installDate = null;

		spiffAmount = 0.00;

		partnerRepUserId = null;

		firstName = null;

		lastName = null;

		isBankInfoExists = false;

		isW9InfoExists = false;

		isContactInfoExists = false;

		invoiceUrl = null;

		invoiceNumber = null;

		facilityName = null;

	}

	public SpiffEntitlementWithPartnerRepInfoWebView(
			@NonNull Long spiffEntitlementId,
			@NonNull Long partnerSaleRegistrationId,
			@NonNull String installDate,
			@NonNull Double spiffAmount,
			@NonNull String partnerRepUserId,
			@NonNull String firstName,
			@NonNull String lastName,
			@NonNull boolean isBankInfoExists,
			@NonNull boolean isW9InfoExists,
			@NonNull boolean isContactInfoExists,
			@NonNull String invoiceUrl,
			@NonNull String invoiceNumber,
			@NonNull String facilityName
			){

		this.spiffEntitlementId =
                guardThat(
                        "spiffEntitlementId",
                        spiffEntitlementId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

		this.installDate =
                guardThat(
                        "installDate",
                        installDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffAmount =
                guardThat(
                        "spiffAmount",
                        spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                        partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

		this.firstName =
                guardThat(
                        "firstName",
                        firstName
                )
                        .isNotNull()
                        .thenGetValue();

		this.lastName =
                guardThat(
                        "lastName",
                        lastName
                )
                        .isNotNull()
                        .thenGetValue();

		this.isBankInfoExists =
                guardThat(
                        "isBankInfoExists",
                        isBankInfoExists
                )
                        .isNotNull()
                        .thenGetValue();


		this.isW9InfoExists =
                guardThat(
                        "isW9InfoExists",
                        isW9InfoExists
                )
                        .isNotNull()
                        .thenGetValue();

		this.isContactInfoExists =
                guardThat(
                        "isContactInfoExists",
                        isContactInfoExists
                )
                        .isNotNull()
                        .thenGetValue();

		this.invoiceUrl =
                guardThat(
                        "invoiceUrl",
                        invoiceUrl
                )
                        .isNotNull()
                        .thenGetValue();

		this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                        invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

		this.facilityName =
                guardThat(
                        "facilityName",
                        facilityName
                )
                        .isNotNull()
                        .thenGetValue();
	}

	/*
	 getter methods
	 */

	public Long getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public String getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public boolean isBankInfoExists() {
		return isBankInfoExists;
	}

	public boolean isW9InfoExists() {
		return isW9InfoExists;
	}

	public boolean isContactInfoExists() {
		return isContactInfoExists;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getFacilityName() {
		return facilityName;
	}

}
