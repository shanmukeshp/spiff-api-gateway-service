package com.precorconnect.spiffgatewayservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class ClaimSpiffWebView {

	/*
	 fields
	*/
	private final Long claimSpiffId;

	private final Long partnerSaleRegistrationId;

	private final String accountId;

	private final String userId;

	private final String firstName;

	private final String lastName;

	private final String installDate;

	private final Double spiffAmount;

	private final String spiffClaimedDate;

	private final String facilityName;

	private final String invoiceNumber;

	/*
	 Constructors
	 */
	public ClaimSpiffWebView(){

		claimSpiffId = 0L;

		partnerSaleRegistrationId = 0L;

		accountId = null;

		userId = null;

		firstName = null;

		lastName = null;

		installDate = null;

		spiffAmount = 0.00;

		spiffClaimedDate = null;

		facilityName = null;

		invoiceNumber = null;

	}

	public ClaimSpiffWebView(
			@NonNull final Long claimSpiffId,
			@NonNull final Long partnerSaleRegistrationId,
			@NonNull final String accountId,
			@NonNull final String userId,
			@NonNull final String firstName,
			@NonNull final String lastName,
			@NonNull final String installDate,
			@NonNull final Double spiffAmount,
			@NonNull final String spiffClaimedDate,
			@NonNull final String facilityName,
			@NonNull final String invoiceNumber
			){

		this.claimSpiffId =
                guardThat(
                        "claimSpiffId",
                        claimSpiffId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

		this.accountId =
                guardThat(
                        "accountId",
                        accountId
                )
                        .isNotNull()
                        .thenGetValue();

		this.userId =
                guardThat(
                        "userId",
                        userId
                )
                        .isNotNull()
                        .thenGetValue();
		this.firstName =
                guardThat(
                        "firstName",
                        firstName
                )
                        .isNotNull()
                        .thenGetValue();

		this.lastName =
                guardThat(
                        "lastName",
                        lastName
                )
                        .isNotNull()
                        .thenGetValue();

		this.installDate =
                guardThat(
                        "installDate",
                        installDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffAmount =
                guardThat(
                        "spiffAmount",
                        spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffClaimedDate =
                guardThat(
                        "spiffClaimedDate",
                        spiffClaimedDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.facilityName =
                guardThat(
                        "facilityName",
                        facilityName
                )
                        .isNotNull()
                        .thenGetValue();

		this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                        invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();
	}

	/*
	 getter methods
	*/
	public Long getClaimSpiffId() {
		return claimSpiffId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getAccountId() {
		return accountId;
	}

	public String getUserId() {
		return userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public String getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

}
