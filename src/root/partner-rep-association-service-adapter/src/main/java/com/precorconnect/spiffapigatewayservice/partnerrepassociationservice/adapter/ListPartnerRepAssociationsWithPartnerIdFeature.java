package com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepAssociationSynopsisView;

interface ListPartnerRepAssociationsWithPartnerIdFeature {

    Collection<PartnerRepAssociationSynopsisView> execute(
            @NonNull AccountId partnerId,
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;

}
