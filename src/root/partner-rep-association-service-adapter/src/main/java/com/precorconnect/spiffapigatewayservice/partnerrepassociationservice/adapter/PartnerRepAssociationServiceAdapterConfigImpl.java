package com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.partnerrepassociationservice.sdk.PartnerRepAssociationServiceSdkConfig;

public class PartnerRepAssociationServiceAdapterConfigImpl
        implements PartnerRepAssociationServiceAdapterConfig {

   
    private final PartnerRepAssociationServiceSdkConfig partnerRepAssociationServiceSdkConfig;


    /*
    constructors
     */
    public PartnerRepAssociationServiceAdapterConfigImpl(
            final @NonNull PartnerRepAssociationServiceSdkConfig partnerRepAssociationServiceSdkConfig
    ) {

      
        this.partnerRepAssociationServiceSdkConfig =
                guardThat(
                        "partnerRepAssociationServiceSdkConfig",
                        partnerRepAssociationServiceSdkConfig
                )
                        .isNotNull()
                        .thenGetValue();

    }

  
    @Override
    public PartnerRepAssociationServiceSdkConfig getPartnerRepAssociationServiceSdkConfig() {
        return partnerRepAssociationServiceSdkConfig;
    }


}
