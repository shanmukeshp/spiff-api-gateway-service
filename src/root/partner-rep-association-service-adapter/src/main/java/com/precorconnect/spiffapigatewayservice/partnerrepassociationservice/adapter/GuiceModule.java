package com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.precorconnect.partnerrepassociationservice.sdk.PartnerRepAssociationServiceSdk;
import com.precorconnect.partnerrepassociationservice.sdk.PartnerRepAssociationServiceSdkImpl;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final PartnerRepAssociationServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull PartnerRepAssociationServiceAdapterConfig config
    ) {

        this.config =
                guardThat(
                        "config",
                        config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {
    	bindFactories();
    	
        bindFeatures();
    }

    private void bindFeatures() {

        bind(ListPartnerRepAssociationsWithPartnerIdFeature.class)
                .to(ListPartnerRepAssociationsWithPartnerIdFeatureImpl.class);

    }
    
    private void bindFactories() {
        bind(PartnerRepAssociationServiceAdapterConfigFactory.class)
        .to(PartnerRepAssociationServiceAdapterConfigFactoryImpl.class);
        bind(PartnerRepAssociationSynopsisViewFactory.class)
        .to(PartnerRepAssociationSynopsisViewFactoryImpl.class);
    }
    

    @Provides
    @Singleton
    PartnerRepAssociationServiceAdapterConfig partnerRepAssociationServiceAdapterConfig() {

        return
                config;

    }

    @Provides
    @Singleton
    PartnerRepAssociationServiceSdk partnerRepAssociationServiceSdk() {

        return
                new PartnerRepAssociationServiceSdkImpl(
                        config
                                .getPartnerRepAssociationServiceSdkConfig()
                );

    }

}
