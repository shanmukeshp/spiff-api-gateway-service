package com.precorconnect.spiffapigatewayservice.api;

import java.util.Collection;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public interface SpiffApiGatewayService {

	Collection<PartnerRepInfoView> listPartnerRepsInfoWithAccountId(
			AccountId partnerAccountId, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

	Collection<SpiffEntitlementWithPartnerRepInfoView> listSpiffEntitlementsAccountId(
			AccountId partnerAccountId, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

	PartnerSaleInvoiceView uploadInvoiceForPartnerSaleRegistration(
			UploadPartnerSaleInvoiceReq req, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

	Collection<ClaimSpiffView> claimSpiffEntitlements(
			AccountId partnerAccountId,
			Collection<SpiffEntitlementView> spiffEntitlements,
			OAuth2AccessToken accessToken) throws AuthenticationException,
			AuthorizationException;
}
