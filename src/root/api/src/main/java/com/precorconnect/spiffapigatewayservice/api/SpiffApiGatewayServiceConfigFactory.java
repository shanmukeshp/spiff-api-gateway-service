package com.precorconnect.spiffapigatewayservice.api;

public interface SpiffApiGatewayServiceConfigFactory {

	SpiffApiGatewayServiceConfig construct();
}
