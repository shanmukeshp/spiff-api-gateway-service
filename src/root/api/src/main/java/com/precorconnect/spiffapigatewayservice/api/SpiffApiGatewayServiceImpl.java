package com.precorconnect.spiffapigatewayservice.api;

import java.util.Collection;

import org.spiff.entitlement.service.adapter.SpiffEntitlementServiceAdapterImpl;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.ClaimSpiffServiceAdapterImpl;
import com.precorconnect.spiffapigatewayservice.core.Core;
import com.precorconnect.spiffapigatewayservice.core.CoreImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;
import com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter.PartnerRepAssociationServiceAdapterImpl;
import com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter.PartnerRepServiceAdapterImpl;
import com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter.PartnerSaleInvoiceServiceAdapterImpl;

public final class SpiffApiGatewayServiceImpl implements SpiffApiGatewayService {

	private Core core;

	public SpiffApiGatewayServiceImpl(final SpiffApiGatewayServiceConfig spiffApiGatewayServiceConfig) {

		this.core = new CoreImpl(new PartnerRepServiceAdapterImpl(spiffApiGatewayServiceConfig.getPartnerRepServiceAdapterConfig()),
				                 new PartnerRepAssociationServiceAdapterImpl(spiffApiGatewayServiceConfig.getPartnerRepAssociationServiceAdapterConfig()),
				                 new SpiffEntitlementServiceAdapterImpl(spiffApiGatewayServiceConfig.getSpiffEntitlementServiceAdapterConfig()),
				                 new PartnerSaleInvoiceServiceAdapterImpl(spiffApiGatewayServiceConfig.getPartnerSaleInvoiceServiceAdapterConfig()),
				                 new ClaimSpiffServiceAdapterImpl(spiffApiGatewayServiceConfig.getClaimSpiffsServiceAdapterConfig()));

	}

	@Override
	public Collection<PartnerRepInfoView> listPartnerRepsInfoWithAccountId(AccountId partnerAccountId,
																		   OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {

		return core.listPartnerRepsInfoWithPartnerAccountId(partnerAccountId, accessToken);
	}

	@Override
	public PartnerSaleInvoiceView uploadInvoiceForPartnerSaleRegistration(
			final UploadPartnerSaleInvoiceReq req, OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException {
		 return core.uploadInvoiceForPartnerSaleRegistration(
				 req,
				 accessToken
				 );
	}

	@Override
	public Collection<SpiffEntitlementWithPartnerRepInfoView> listSpiffEntitlementsAccountId(AccountId partnerAccountId,
			OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {
		return core.listSpiffEntitlementsWithPartnerAccountId(partnerAccountId, accessToken);
	}

	@Override
	public Collection<ClaimSpiffView> claimSpiffEntitlements(
			AccountId partnerAccountId,
			Collection<SpiffEntitlementView> spiffEntitlements,
			OAuth2AccessToken accessToken) throws AuthenticationException,
			AuthorizationException {
		return core.claimSpiffEntitlements(partnerAccountId, spiffEntitlements, accessToken);
	}


}
