package com.precorconnect.spiffapigatewayservice.api;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.spiff.entitlement.service.adapter.SpiffEntitlementServiceAdapterConfig;

import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.ClaimSpiffsServiceAdapterConfig;
import com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter.PartnerRepAssociationServiceAdapterConfig;
import com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter.PartnerRepServiceAdapterConfig;
import com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter.PartnerSaleInvoiceServiceAdapterConfig;

public final class SpiffApiGatewayServiceConfigImpl implements SpiffApiGatewayServiceConfig {
	
	private final PartnerRepServiceAdapterConfig partnerRepServiceAdapterConfig;
	
	private final PartnerRepAssociationServiceAdapterConfig partnerRepAssociationServiceAdapterConfig;
	
	private final PartnerSaleInvoiceServiceAdapterConfig partnerSaleInvoiceServiceAdapterConfig;
	
	private final SpiffEntitlementServiceAdapterConfig spiffEntitlementServiceAdapterConfig;
	
	private final ClaimSpiffsServiceAdapterConfig claimSpiffsServiceAdapterConfig;
	
	
	public SpiffApiGatewayServiceConfigImpl(final PartnerRepServiceAdapterConfig partnerRepServiceAdapterConfig,
											final PartnerRepAssociationServiceAdapterConfig partnerRepAssociationServiceAdapterConfig,
											final PartnerSaleInvoiceServiceAdapterConfig partnerSaleInvoiceServiceAdapterConfig,
											final SpiffEntitlementServiceAdapterConfig spiffEntitlementServiceAdapterConfig,
											final ClaimSpiffsServiceAdapterConfig claimSpiffsServiceAdapterConfig) {
		this.partnerRepAssociationServiceAdapterConfig = guardThat(
                											"partnerRepAssociationServiceAdapterConfig",
                											partnerRepAssociationServiceAdapterConfig
														 )
														 	.isNotNull()
														 	.thenGetValue();
		

		
		this.partnerRepServiceAdapterConfig = guardThat(
				"partnerRepServiceAdapterConfig",
				partnerRepServiceAdapterConfig
			 )
			 	.isNotNull()
			 	.thenGetValue();

		
		this.partnerSaleInvoiceServiceAdapterConfig = guardThat(
				"partnerSaleInvoiceServiceAdapterConfig",
				partnerSaleInvoiceServiceAdapterConfig
			 )
			 	.isNotNull()
			 	.thenGetValue();

		
		this.spiffEntitlementServiceAdapterConfig = guardThat(
				"spiffEntitlementServiceAdapterConfig",
				spiffEntitlementServiceAdapterConfig
			 )
			 	.isNotNull()
			 	.thenGetValue();

		this.claimSpiffsServiceAdapterConfig = guardThat(
				"claimSpiffsServiceAdapterConfig",
				claimSpiffsServiceAdapterConfig
			 )
			 	.isNotNull()
			 	.thenGetValue();

	}

	@Override
	public PartnerRepServiceAdapterConfig getPartnerRepServiceAdapterConfig() {
		return partnerRepServiceAdapterConfig;
	}

	@Override
	public PartnerRepAssociationServiceAdapterConfig getPartnerRepAssociationServiceAdapterConfig() {
		return partnerRepAssociationServiceAdapterConfig;
	}

	@Override
	public PartnerSaleInvoiceServiceAdapterConfig getPartnerSaleInvoiceServiceAdapterConfig() {
		return partnerSaleInvoiceServiceAdapterConfig;
	}

	@Override
	public SpiffEntitlementServiceAdapterConfig getSpiffEntitlementServiceAdapterConfig() {
		return spiffEntitlementServiceAdapterConfig;
	}

	@Override
	public ClaimSpiffsServiceAdapterConfig getClaimSpiffsServiceAdapterConfig() {
		return claimSpiffsServiceAdapterConfig;
	}

}
