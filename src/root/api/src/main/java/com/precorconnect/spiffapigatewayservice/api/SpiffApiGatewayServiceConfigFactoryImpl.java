package com.precorconnect.spiffapigatewayservice.api;

import org.spiff.entitlement.service.adapter.SpiffEntitlementServiceAdapterConfigFactoryImpl;

import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.ClaimSpiffsServiceAdapterConfigFactoryImpl;
import com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter.PartnerRepAssociationServiceAdapterConfigFactoryImpl;
import com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter.PartnerRepServiceAdapterConfigFactoryImpl;
import com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter.PartnerSaleInvoiceServiceConfigAdapterFactoryImpl;

public final class SpiffApiGatewayServiceConfigFactoryImpl implements SpiffApiGatewayServiceConfigFactory {

	@Override
	public SpiffApiGatewayServiceConfig construct() {
		return new SpiffApiGatewayServiceConfigImpl(new PartnerRepServiceAdapterConfigFactoryImpl().construct(),
				new PartnerRepAssociationServiceAdapterConfigFactoryImpl().construct(),
				new PartnerSaleInvoiceServiceConfigAdapterFactoryImpl().construct(),
				new SpiffEntitlementServiceAdapterConfigFactoryImpl().construct(),
				new ClaimSpiffsServiceAdapterConfigFactoryImpl().construct());
	}

}
