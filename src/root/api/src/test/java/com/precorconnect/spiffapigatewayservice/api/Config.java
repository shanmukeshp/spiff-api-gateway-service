package com.precorconnect.spiffapigatewayservice.api;

import static com.precorconnect.guardclauses.Guards.guardThat;

import com.precorconnect.identityservice.HmacKey;

public class Config {

	/*
	 * fields
	 */
	
	private final SpiffApiGatewayServiceConfig spiffApiGatewayServiceConfig;
	
	 private final HmacKey identityServiceJwtSigningKey;

	public Config(
			SpiffApiGatewayServiceConfig spiffApiGatewayServiceConfig,
			 HmacKey identityServiceJwtSigningKey
		) {
		
		this.spiffApiGatewayServiceConfig =
                guardThat("spiffApiGatewayServiceConfig",
                		spiffApiGatewayServiceConfig
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.identityServiceJwtSigningKey =
                guardThat("identityServiceJwtSigningKey",
                        identityServiceJwtSigningKey
                )
                        .isNotNull()
                        .thenGetValue();
	}
	
	
	/*
	 * getter methods
	 */
	
	public SpiffApiGatewayServiceConfig getSpiffApiGatewayServiceConfig() {
		return spiffApiGatewayServiceConfig;
	}


	public HmacKey getIdentityServiceJwtSigningKey() {
		return identityServiceJwtSigningKey;
	}
	
	
	
}
