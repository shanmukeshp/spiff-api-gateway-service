package com.precorconnect.spiffapigatewayservice.api;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.FileExtension;
import com.precorconnect.FileExtensionImpl;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastNameImpl;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.PartnerSaleInvoiceIdImpl;
import com.precorconnect.PartnerSaleInvoiceNumber;
import com.precorconnect.PartnerSaleInvoiceNumberImpl;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.PartnerSaleRegIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityName;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceInfoImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceNumber;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffClaimedDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffClaimedDateImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReqImpl;



public class Dummy {

	private URI uri;

	private AccountId accountId=new AccountIdImpl("123456789012345678");

	private OAuth2AccessToken accessToken=null;

	private UploadPartnerSaleInvoiceReq uploadPartnerSaleInvoiceReq;

	private Collection<SpiffEntitlementView> spiffEntitlements=new ArrayList<SpiffEntitlementView>();

	private SpiffEntitlementView spiffEntitlementView;

	private PartnerSaleRegId partnerSaleRegistrationId=new  PartnerSaleRegIdImpl(12L);

	private InstallDate installDate;

	private SpiffAmount spiffAmount=new SpiffAmountImpl(100.00);

	private  SpiffEntitlementId spiffEntitlementId=new SpiffEntitlementIdImpl(1L);

	private PartnerRepInfoView partnerRepInfoView;

	private InvoiceInfo invoiceInfo;

	private FacilityName facilityName=new FacilityNameImpl("ABC");

	private UserId userId =new UserIdImpl("123");

	private String firstName=new String("john");

	private String lastName=new String("smith");

	private boolean isBankInfoExists=true;

	private boolean isW9InfoExists=true;

	private boolean isContactInfoExists=true;

	private InvoiceUrl invoiceUrl=new InvoiceUrlImpl("www.testinvoice.com");

	private InvoiceNumber invoiceNumber=new InvoiceNumberImpl("1234");

	private ClaimSpiffId claimSpiffId=new ClaimSpiffIdImpl(12L);

	private PartnerSaleInvoiceView partnerSaleInvoiceView;

	private Collection<ClaimSpiffSynopysisViewImpl> claimSpiffViewList=new ArrayList<ClaimSpiffSynopysisViewImpl>();

	private ClaimSpiffSynopysisViewImpl claimSpiffView;

	private SpiffClaimedDate spiffClaimedDate;

	private FileExtension fileExtension=new FileExtensionImpl("PDF");

	private PartnerSaleInvoiceNumber partnerSaleInvoiceNumber=new PartnerSaleInvoiceNumberImpl("12345");

	private PartnerSaleInvoiceId partnerSaleInvoiceId=new PartnerSaleInvoiceIdImpl("123456789012345678901234567890123456");


	private PartnerSaleRegId partnerSaleRegId=new PartnerSaleRegIdImpl(12L);

	private URL fileUrl;

	 private final File file;

	public Dummy() {

		try {

            uri = new URI("http://dev.precorconnect.com");

            file = new File("test-file.png");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

		try{

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date;
		try {
			date = formatter.parse("11/21/1985");
		} catch (ParseException e) {
			throw new RuntimeException("Date field parse exception: ",e);
		}

		fileUrl=new URL("https://api-dev.precorconnect.com");

		installDate = new com.precorconnect.spiffapigatewayservice.objectmodel.InstallDateImpl(date);

		spiffClaimedDate= new SpiffClaimedDateImpl(date);

		partnerRepInfoView=
				new PartnerRepInfoViewImpl(
						userId,
						firstName,
						lastName,
						isBankInfoExists,
						isW9InfoExists,
						isContactInfoExists
				);

		invoiceInfo=
				new InvoiceInfoImpl(
						invoiceUrl,
						invoiceNumber
				);


		spiffEntitlementView=
				new SpiffEntitlementViewImpl(
						partnerSaleRegistrationId,
						installDate,
						spiffAmount,
						userId,
						new FirstNameImpl(firstName),
						new LastNameImpl(lastName),
						invoiceInfo,
						facilityName,
						spiffEntitlementId
					);



		spiffEntitlements.add(spiffEntitlementView);

		claimSpiffView=
				new ClaimSpiffSynopysisViewImpl(
						claimSpiffId,
						partnerSaleRegistrationId,
						accountId,
						userId,
						new FirstNameImpl(firstName),
						new LastNameImpl(lastName),
						installDate,
						spiffAmount,
						spiffClaimedDate,
						facilityName,
						invoiceNumber
					);

		claimSpiffViewList.add(claimSpiffView);



		uploadPartnerSaleInvoiceReq=
				new UploadPartnerSaleInvoiceReqImpl(
						partnerSaleInvoiceNumber,
						partnerSaleRegId,
						file
						);

		partnerSaleInvoiceView=
				new PartnerSaleInvoiceViewImpl(
						partnerSaleInvoiceId,
						partnerSaleInvoiceNumber,
						fileUrl,
						partnerSaleRegId
					);

		}catch(MalformedURLException e){
			throw new RuntimeException(e);
		}
	}




	public AccountId getAccountId() {
		return accountId;
	}




	public OAuth2AccessToken getAccessToken() {
		return accessToken;
	}




	public UploadPartnerSaleInvoiceReq getUploadPartnerSaleInvoiceReq() {
		return uploadPartnerSaleInvoiceReq;
	}




	public Collection<SpiffEntitlementView> getSpiffEntitlements() {
		return spiffEntitlements;
	}




	public SpiffEntitlementView getSpiffEntitlementView() {
		return spiffEntitlementView;
	}




	public PartnerSaleRegId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}




	public InstallDate getInstallDate() {
		return installDate;
	}




	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}




	public PartnerRepInfoView getPartnerRepInfoView() {
		return partnerRepInfoView;
	}




	public InvoiceInfo getInvoiceInfo() {
		return invoiceInfo;
	}




	public FacilityName getFacilityName() {
		return facilityName;
	}




	public UserId getUserId() {
		return userId;
	}




	public String getFirstName() {
		return firstName;
	}




	public String getLastName() {
		return lastName;
	}




	public boolean isBankInfoExists() {
		return isBankInfoExists;
	}




	public boolean isW9InfoExists() {
		return isW9InfoExists;
	}




	public boolean isContactInfoExists() {
		return isContactInfoExists;
	}




	public InvoiceUrl getInvoiceUrl() {
		return invoiceUrl;
	}




	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}




	public FileExtension getFileExtension() {
		return fileExtension;
	}




	public PartnerSaleInvoiceView getPartnerSaleInvoiceView() {
		return partnerSaleInvoiceView;
	}




	public Collection<ClaimSpiffSynopysisViewImpl> getClaimSpiffViewList() {
		return claimSpiffViewList;
	}




	public SpiffEntitlementId getSpiffEntitlementId() {
		return spiffEntitlementId;
	}




	public ClaimSpiffId getClaimSpiffId() {
		return claimSpiffId;
	}




	public ClaimSpiffSynopysisViewImpl getClaimSpiffView() {
		return claimSpiffView;
	}




	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}




	public PartnerSaleInvoiceNumber getPartnerSaleInvoiceNumber() {
		return partnerSaleInvoiceNumber;
	}




	public PartnerSaleInvoiceId getPartnerSaleInvoiceId() {
		return partnerSaleInvoiceId;
	}




	public PartnerSaleRegId getPartnerSaleRegId() {
		return partnerSaleRegId;
	}




	public URL getFileUrl() {
		return fileUrl;
	}




	public URI getUri() {
		return uri;
	}




	public File getFile() {
		return file;
	}








}
